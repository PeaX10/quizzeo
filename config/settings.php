<?php

return [
    'fonts' => [
        public_path('fonts/pacifico/Pacifico.ttf') => 'Pacifico',
        public_path('fonts/allura/Allura-Regular.otf') => 'Allura Regular',
        public_path('fonts/amatic/AmaticSC-Regular.ttf') => 'Amatic Regular',
        public_path('fonts/amatic/Amatic-Bold.ttf') => 'Amatic Bold',
        public_path('fonts/ChunkFive/Chunkfive.otf') => 'ChunkFive',
        public_path('fonts/GoodDog/GoodDog.otf') => 'GoodDog',
        public_path('fonts/great-vibes/GreatVibes-Regular.otf') => 'GreatVibes Regular',
        public_path('fonts/kaushan-script/KaushanScript-Regular.otf') => 'KaushanScript',
        public_path('fonts/dafont/LemonMilk.otf') => 'Lemon Milk',
        public_path('fonts/century-gothic.ttf') => 'Century Gothic',
        public_path('fonts/abeatbykai/abeatbyKaiRegular.otf') => 'abeatbyKai',
        public_path('fonts/inglobal/inglobal.ttf') => 'inglobal',
        public_path('fonts/inglobal/inglobal.ttf') => 'inglobal',
        public_path('fonts/inglobal/inglobalb.ttf') => 'inglobal bold',
        public_path('fonts/inglobal/inglobalbi.ttf') => 'inglobal bold italic',
        public_path('fonts/inglobal/inglobali.ttf') => 'inglobal italic',
        public_path('fonts/gabriola.ttf') => 'Gabriola',
        public_path('fonts/mf_really/MfReallyAwesome.ttf') => 'MfReally',
        public_path('fonts/kg_miss_kindergarten/KGMissKindergarten.ttf') => 'KGMissKindergarten',
    ],
    'modules' => [
        'fb_me',
        'fb_friends',
        'carbon',
    ],
    'langs' => [
        'fr' => 'Français',
        'en' => 'English',
        'ar' => 'العربية',
    ],
    'soon_langs' => [
        'it' => 'Italian',
    ],
    'fake_profil' => [
        0 => [
            'id' => 1,
            'name' => 'Mathieu Dupond',
            'gender' => 'male',
            'email' => 'mathieu.dupond@gmail.com',
            'avatar' => 'http://cdn1-elle.ladmedia.fr/var/plain_site/storage/images/beaute/news-beaute/parfums/on-a-rencontre-vinnie-woolston-le-beau-gosse-de-la-nuit-de-l-homme-2980853/56038205-1-fre-FR/On-a-rencontre-Vinnie-Woolston-le-beau-gosse-de-La-Nuit-de-L-Homme.jpg',
            'firstname' => 'Mathieu',
            'name' => 'Dupond'
        ]
    ]
];