@extends('design.layouts.default', ['active' => 'quizz'])

@section('title') Design de la réponse @endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.ui.rotatable/1.0.1/jquery.ui.rotatable.css">
    <link rel="stylesheet" href="{{ url('css/admin.css') }}" type="text/css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="content">
                    <h4>Aperçu de la réponse <a class="btn btn-info btn-fill pull-right" href="{{ url('quizzes/'.$answer->quizz_id.'/edit') }}">Retour au quizz</a></h4>
                    <hr>
                    <figure>
                        <div class="preview img-responsive" style="border:1px solid #EEE; background: @if(file_exists(public_path('uploads/answer/preview/p_'.$answer->id.'.jpg')))url('{{ url('uploads/answer/preview/p_'.$answer->id.'.jpg') }}')@elseif(file_exists(public_path('uploads/answer/preview/'.$answer->id.'.jpg')))url('{{ url('uploads/answer/preview/'.$answer->id.'.jpg') }}')@else #FFF @endif no-repeat center; background-size: contain;">
                        </div>
                    </figure>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="content">
                    <h4>Modifier fond</h4>
                    <hr>
                    {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->action( url('quizzes/'.$answer->quizz_id.'/design/'.$answer->id) )->put()->enctype("multipart/form-data") !!}
                    <div class="form-group">
                        <label class="col-sm-4 col-lg-2 control-label" for="visible">Fond</label>
                        <div class="col-sm-8 col-lg-10">
                            <label><small>Taille stricte 1200x630 (8MB MAX)</small></label>
                            <input 	type='file'
                                      class='input-ghost'
                                      name='image'
                                      style='visibility:hidden; height:0'
                                      onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                            <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                <input 	type="text"
                                          class="form-control"
                                          placeholder='Choisissez un fichier...'
                                          style="cursor:pointer"
                                          onclick="$(this).parents('.input-file').prev().click(); return false;"
                                />
                            </div>
                        </div>
                    </div>
                    {!! BootForm::submit('Modifier')->class('btn btn-info btn-rounded btn-fill pull-right') !!}
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="content">
                    <h4>Builder</h4>
                    <hr>
                    <figure>
                        <div class="preview" style="border:1px solid #EEE; background: url('@if(empty($background)){{ url('uploads/answer/'.$answer->background) }}@else(){{ url('uploads/answer/'.$background) }}@endif') no-repeat center; background-size: contain;">
                            @foreach($composants as $composant)
                                <div id="{{ $composant->id }}" class="composant {{ $composant->type }}"
                                    style="width:{{ $composant->width }}%; height: {{ $composant->height }}%; top: {{ $composant->pos_y }}%;left: {{ $composant->pos_x }}%; z-index: {{ isset($composant->index) ? $composant->index : 0 }}; transform: rotate({{ -1*$composant->angle }}deg); cursor: help">
                                    <a>{{ $composant->name }}</a>
                                </div>
                            @endforeach
                        </div>
                    </figure>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="content">
                    <h4>Composant</h4>
                    <div class="content table-responsive table-full-width">
                        <table id="bootstrap-table" class="table">
                            <tbody>
                                <tr>
                                    <td><b>Width:</b></td>
                                    <td><span id="c-width">...</span>px</td>
                                </tr>
                                <tr>
                                    <td><b>Height:</b></td>
                                    <td><span id="c-height">...</span>px</td>
                                </tr>
                                <tr>
                                    <td><b>X / Y:</b></td>
                                    <td><span id="c-x">...</span>px / <span id="c-y">...</span>px</td>
                                </tr>
                                <tr>
                                    <td><b>Angle:</b></td>
                                    <td><span id="c-angle">...</span>deg</td>
                                </tr>
                                <tr>
                                    <td><b>Index:</b></td>
                                    <td><span id="c-index">...</span></td>
                                </tr>
                            </tbody>
                        </table>
                        @if(empty($composant))
                            <h5 align="center">Ce design est vide</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="{{ url('js/jquery.ui.rotatable.min.js') }}"></script>
    <script type="text/javascript">
        $('.preview').height($('.preview').width() * 0.525);

        $(window).on('resize ready', function() {
            $('.preview').height($('.preview').width() * 0.525);
        });

        function convert_to_percenquizze(el, to_array = false){
            var parent = el.parent();
            var info = {
                left: parseInt(el.css('left'))/parent.width()*100+"%",
                top: parseInt(el.css('top'))/parent.height()*100+"%",
                width: el.width()/parent.width()*100+"%",
                height: el.height()/parent.height()*100+"%"
            };
            if(to_array){
                return info;
            }else{
                el.css(info);
            }
        }

        function setContainerSize(el) {
            var parent = $(el.target).parent().parent();
            parent.css('height', parent.height() + "px");
        }

        function getRotationDegrees(obj) {
            var matrix = obj.css("-webkit-transform") ||
                    obj.css("-moz-transform")    ||
                    obj.css("-ms-transform")     ||
                    obj.css("-o-transform")      ||
                    obj.css("transform");
            if(matrix !== 'none') {
                var values = matrix.split('(')[1].split(')')[0].split(',');
                var a = values[0];
                var b = values[1];
                var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
            } else { var angle = 0; }
            return (angle < 0) ? angle + 360 : angle;
        }

        $( ".composant" ).click(function() {
            $('#c-width').html(Math.round($(this).width()/$(this).parent().width()*1200).toFixed(0));
            $('#c-height').html(Math.round($(this).height()/$(this).parent().height()*630).toFixed(0));
            $('#c-x').html(Math.round(parseInt($(this).css('left'))/$(this).parent().width()*1200).toFixed(0));
            $('#c-y').html(Math.round(parseInt($(this).css('top'))/$(this).parent().height()*630).toFixed(0));
            $('#c-angle').html(-1*getRotationDegrees($(this)));
            $('#c-index').html($(this).css('z-index'));
        });
        function savPos(){
            var pos = [];
            var c_pos = [];
            $('.composant').each(function(){
                c_pos = {
                    id: $(this).attr('id'),
                    left: parseInt($(this).css('left'))/$(this).parent().width()*100,
                    top: parseInt($(this).css('top'))/$(this).parent().height()*100,
                    width: $(this).width()/$(this).parent().width()*100,
                    height: $(this).height()/$(this).parent().height()*100,
                    angle: -1*getRotationDegrees($(this)),
                    index: $(this).css('z-index')
                };
                pos.push(c_pos);
            });
            $.ajax({
                url: '{{ Request::url() }}/pos',
                type: 'post',
                data: {_method: 'post', _token : '{{ csrf_token() }}', pos: pos },
                success: function(result) {
                    swal("Positions sauvegardées!", "Les positions des composants ont bien été sauvegardées.", "success");
                },
                error: function(error){
                    console.log(error);
                    swal("Erreur!", "Une erreur c'est produite, les positions des composants n'ont pas été sauvegardées.", "error");
                }
            });
        }

        @if(Session::has('updated_background'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre design a bien envoyé à l'administrateur."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        /*
        var myVar=setInterval(function(){
            $('#info').html('LEFT : '+$('.composant')[0].style.left + '<br />TOP:  '+$('.composant')[0].style.top + '<br />WIDTH: '+$('.composant')[0].style.width + '<br />HEIGHT: '+$('.composant')[0].style.height);
        },10);
        */
    </script>
@endsection