<div class="sidebar" data-color="red">
    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="logo">
        <a href="{{ url('/') }}" class="logo-text">
            Quizzeo
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ url('uploads/avatar/'.$user->avatar) }}" />
            </div>
        </div>

        <ul class="nav">
            <li @if(empty($active)) class="active" @endif>
                <a href="{{ url('/') }}">
                    <i class="pe-7s-graph"></i>
                    <p>Tableau de bord</p>
                </a>
            </li>
            <li @if($active == "translation") class="active" @endif>
                <a href="{{ url('trans') }}">
                    <i class="fa fa-globe"></i>
                    <p>Traductions</p>
                </a>
            </li>
        </ul>
    </div>
</div>