@extends('front.layouts.default')

@section('title') {{ $pagequizz->translation->title }} - MEGATAG @endsection

@section('meta')
    <meta property="fb:app_id" content="1171151359613962">
    <meta property="og:site_name" content="MEGATAG.NET">
    <meta property="og:type" content="website">
    <meta property="og:title"         content="{{ $pagequizz->translation->title }}" />
    <meta property="og:description"   content="{{ $pagequizz->translation->playDescription }}" />
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:image"         content="{{ url('uploads/quizz/'.$pagequizz->playImage) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $pagequizz->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/quizz/'.$pagequizz->playImage) }}">
    <meta name="author" content="MEGATAG">
@endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container">
            <div class="col-md-8 hidden-xs hidden-sm" style="margin-bottom: 5px" onclick="play_quiz('{{ url('auth/facebook') }}')" id="quiz_before_play_section">
                <div class="text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; border: 15px solid #fff; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; display: block" id="view_quiz_section_1">
                    <div style="width:80%; margin: 0px auto" class="text-center">
                        <h3 class="text text-center" style="font-weight: 700; color: #000; font-size: 35px; margin:20px 0;'">
                            <strong>{{ $pagequizz->translation->title }}</strong>
                        </h3>
                        <p>{{ $pagequizz->translation->playDescription }}</p>
                    </div>

                    <div style="width:80%; background: url('{{ url('uploads/quizz/'.$pagequizz->playImage) }}'); background-position: center center; background-size: cover; border-radius: 5px; border:0px solid #000; position: relative; margin: 0px auto; height: 190px; width:362px">
                    </div>

                    <button class="btn btn-primary" style="height:60px; margin: 20px 0; padding: 0 50px !important; font-size: 30px; font-weight: 700; background: #3b5998; box-shadow: 0 4px 4px -2px rgba(0,0,0,.25); border-radius: 6px; color:#FFF">
                        <i class="fa fa-facebook-official"></i>&nbsp;&nbsp;
                        @lang('app.do_quizz')
                    </button>

                    <br /><span style="color:#999; font-size: 12px">@lang('app.fb_cond_text')</span>

                </div>

                <div class="text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; border: 15px solid #fff; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; display: none" id="view_quiz_section_2">

                    <p style="font-size: 40px; margin-top: 40px">

                        <img src="{{asset('img/ripple4.svg')}}">
                    </p>
                    <h3 style="font-size: 32px">@lang('app.analysing_profile') <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row col-md-8 col-md-offset-2" style="border: 1px solid #336699; border-radius: 5px;">
                        <div class="row">
                            <div class="col-md-12" style="background: #336699; padding: 15px; color:#fff; font-size: 20px"> @lang('app.hit_like') </div>
                        </div>

                        <div class="row" style="background:#fff; padding-bottom: 20px; border-radius: 5px">
                            <br />
                            <div class="col-md-2">
                                <img src="{{url('img/logo-navbar.png')}}" class="img img-responsive">
                            </div>
                            <div class="col-md-10">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-width="300" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; border: 15px solid #fff; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; display: none" id="view_quiz_section_3">

                    <p style="font-size: 40px; margin-top: 40px">

                        <img src="{{ url('img/ripple4.svg') }}">
                    </p>
                    <h3 style=" font-size: 32px">@lang('app.generating_profile') <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row col-md-8 col-md-offset-2" style="border: 1px solid #336699; border-radius: 5px;">
                        <div class="row">
                            <div class="col-md-12" style="background: #336699; padding: 15px; color:#fff; font-size: 20px"> @lang('app.hit_like') </div>
                        </div>

                        <div class="row" style="background:#fff; padding-bottom: 20px; border-radius: 5px">
                            <br />
                            <div class="col-md-2">
                                <img src="{{ url('img/logo-navbar.png') }}" class="img img-responsive">
                            </div>
                            <div class="col-md-10">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-width="300" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12 col-xs-12 hidden-md hidden-lg" style="margin-bottom: 5px" onclick="play_quiz_mobile('{{ url('auth/facebook') }}')" id="quiz_before_play_section_m">
                <div class="text-center" style="width: 100%; height: auto; background-color: #cce4ff; border-radius: 5px; border: 5px solid #fff; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; display: block" id="view_quiz_section_m_1">
                    <div style="width:80%; margin: 0px auto" class="text-center">
                        <h3 class="text text-center" style="font-weight: 700; color: #000; font-size: 25px; margin:20px 0;">
                            <strong>{{ $pagequizz->translation->title }}</strong>
                        </h3>
                    </div>

                    <img src="{{ url('uploads/quizz/'.$pagequizz->playImage) }}" class="img img-responsive" style="padding: 10px">
                    <center>
                        <button class="btn btn-primary" style="height:60px; margin: 10px auto; padding: 0 25px !important; font-size: 16px; font-weight: 700; background: #3b5998; box-shadow: 0 4px 4px -2px rgba(0,0,0,.25); border-radius: 6px; color:#FFF;" onclick="show_loader()">
                            <i class="fa fa-facebook-official"></i>&nbsp;@lang('app.do_quizz')
                        </button>
                    </center>
                    <br />
                    <p style="color:#999; font-size: 10px; margin-bottom: 20px">@lang('app.fb_cond_text')</p>

                </div>
                <div class="text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; border: 15px solid #fff; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; display: none" id="view_quiz_section_m_2">

                    <p style="font-size: 40px; margin-top: 30px">

                        <img src="{{ url('img/ripple4.svg') }}" style="width:40%">
                    </p>
                    <h3 style=" font-size: 25px">@lang('app.analysing_profile') <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row" style="border: 1px solid #336699; border-radius: 5px; margin: 0 10px">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="background: #336699; padding: 0 5px; color:#fff; font-size: 20px"> @lang('app.hit_like') </div>

                        <br />
                        <div class="col-sm-12 col-xs-12" style="background: #fff; border-radius: 5px">
                            <div style="padding: 10px 0">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-width="80" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; border: 15px solid #fff; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; display: none" id="view_quiz_section_m_3">

                    <p style="font-size: 40px; margin-top: 30px">

                        <img src="{{ url('img/ripple4.svg') }}" style="width:40%">
                    </p>
                    <h3 style=" font-size: 25px">@lang('app.generating_profile') <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row" style="border: 1px solid #336699; border-radius: 5px; margin: 0 10px">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="background: #336699; padding: 0 5px; color:#fff; font-size: 20px"> @lang('app.hit_like') </div>

                        <br />
                        <div class="col-sm-12 col-xs-12" style="background: #fff; border-radius: 5px">
                            <div style="padding: 10px 0">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-width="80" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="col-md-4 items-row hidden-xs hidden-sm">

                @foreach($quizzs->random(2) as $quizz)
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center>
                            <a class="text-center" id="quiz_link" href="{{ url('q/'.$quizz->slug) }}" onclick="show_loader('{{ $quizz->slug }}')">
                                <div class="text-center" style="padding: 5px; margin-bottom:0px; border-radius: 5px; border:0px solid #fff; width: 100%; height: 180px; background-image: url('{{ url('uploads/quizz/'.$quizz->image) }}'); background-size: cover; background-position: center center">
                                </div>
                                <p style="text-align: left">
                                    <span style="color:#000; font-size: 16px">{{ $quizz->translation->title }}</span>
                                </p>
                            </a>
                        </center>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-nude-gray">
        <div class="container">
            <div class="row items-row col-md-12 quizzs">
                @include('front.pages.quizzs', ['tags' => $quizzs])
            </div>
        </div>
        <div class="container">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center">
                <button id="btn_load_more" class="btn btn-primary" style="width: 90%; padding: 15px; font-size: 23px; font-weight: bold; margin-top: 10px; color: #fff;background-color: #3097D1;border-color: #2a88bd;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;
                    @lang('app.discover_more')
                </button>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @if(Auth::check() && $pagequizz->type == 'friend')
        <div class="modal fade" id="friendModal" tabindex="-1" role="dialog" aria-labelledby="friendModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form action="{{ url('dq/'.$pagequizz->slug) }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="friendModalLabel">@lang('app.choose_friend')</h4>
                        </div>
                        <div class="modal-body">
                            <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="friend" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                @foreach($friends as $friend)
                                    <option value="{{ $friend->name }}" data-content="<img src='{{ $friend->picture }}' class='img-circle img-select' style='margin-right:10px' />{{ $friend->name }}">{{ $friend->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="data" id="data_friend">
                            <hr>
                            @lang('app.any_problems') <a id="syncFriend" href="{{ url('sync/friends') }}">@lang('app.sync_facebook_friends')</a>
                        </div>
                        <div class="modal-footer">
                            <div class="left-side">
                                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">@lang('app.cancel')</button>
                            </div>
                            <div class="divider"></div>
                            <div class="right-side">
                                <input type="submit" class="btn btn-info btn-simple" value="@lang('app.confirm')">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function play_quiz(login_url){
            $('#quiz_before_play_section').attr('onclick', '');
            $('#view_quiz_section_1').hide();
            $('#view_quiz_section_2').show();
            setTimeout( function(){
                $('#view_quiz_section_2').hide();
                $('#view_quiz_section_3').show();
            }  , 3000 );
            setTimeout( function(){
                window.location.href = login_url;
            }  , 4000 );
        }

        function play_quiz_mobile(login_url){
            $('#quiz_before_play_section_m').attr('onclick', '');
            $('#view_quiz_section_m_1').hide();
            $('#view_quiz_section_m_2').show();
            setTimeout( function(){
                $('#view_quiz_section_m_2').hide();
                $('#view_quiz_section_m_3').show();
            }  , 3000 );
            setTimeout( function(){
                window.location.href = login_url;
            }  , 4000 );
        }
    </script>
@endsection