@extends('front.layouts.default')

@section('meta')
    <meta property="fb:app_id" content="1171151359613962">
    <meta property="og:site_name" content="MEGATAG.NET">
    <meta property="og:type" content="website">
    <meta property="og:title"         content="{{ $result->quizz->translation->title }}" />
    <meta property="og:description"   content="{{ $result->quizz->translation->resultDescription }}" />
    <meta property="og:url"           content="{{ url('qrs/'.$result->quizz->slug) }}" />
    <meta property="og:image"         content="{{ url('uploads/result/'.$result->image) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $result->quizz->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/result/'.$result->image) }}">
    <meta name="author" content="{{ $result->user->name }}">
@endsection
@section('title') {{ $result->quizz->translation->title }} - MEGATAG @endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container">
            <div class="col-md-8 col-sm-12 col-xs-12" style="background: #fff; border-radius: 5px; padding-top: 20px; padding-bottom:20px; -webkit-box-shadow: 0 0 10px #ccc !important; box-shadow: 0 0 10px #ccc !important; cursor: pointer">
                <div id="view_result_section_1">
                    <center>
                        <div class="hidden-xs">
                            <div class="share_bt" onclick="share(1)">
                                <img src="{{url('img/facebook_logo.png')}}" class="share_bt_fb" />
                                @lang('app.quizz_share_fb')
                            </div>
                            <div class="share_bt sbt_twitter" onclick="share(2)">
                                <img src="{{url('img/twitter_icon.png')}}" class="share_bt_fb sbtfb_twitter"  />
                            </div>
                            <div class="share_bt sbt_wa"  onclick="share(3)">
                                <img src="{{url('img/wa_logo_2.png')}}" class="share_bt_fb sbtfb_twitter" />
                            </div>
                        </div>
                        <!--  Mobile -->
                        <div class="hidden-md hidden-lg hidden-sm">
                            <div class="share_bt_mb" onclick="share(1)">
                                <img src="{{url('img/facebook_logo.png')}}" class="share_bt_fb sbtfb_twitter"  style="position: relative;"/>
                                Share
                            </div>
                            <div class="share_bt sbt_twitter" onclick="share(2)">
                                <img src="{{url('img/twitter_icon.png')}}" class="share_bt_fb sbtfb_twitter" />
                            </div>
                            <div class="share_bt_mb sbt_wa_mb" onclick="share(3)">
                                <img src="{{url('img/wa_logo_2.png')}}" class="share_bt_fb sbtfb_twitter"  />
                            </div>
                        </div>
                    </center>
                    <p>
                        <img src="{{ url('uploads/result/'.$result->image) }}" class="img img-responsive" onclick="share(1)" />
                    </p>
                    <p class="" style="font-size: 20px; color:#000">
                        {{$result->quizz->translation->resultDescription }}
                    </p>
                    <p class="text-center">
                    <center><div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false" data-width="80"></div></center>
                    </p>
                    <p>
                    <center>
                        <div class="hidden-xs" data-toggle="sticky-onscroll">
                            <div class="share_bt" onclick="share(1)">
                                <img src="{{url('img/facebook_logo.png')}}" class="share_bt_fb"  />
                                @lang('app.quizz_share_fb')
                            </div>
                            <div class="share_bt sbt_twitter" onclick="share(2)">
                                <img src="{{url('img/twitter_icon.png')}}" class="share_bt_fb sbtfb_twitter"  />
                            </div>
                            <div class="share_bt sbt_wa" onclick="share(3)">
                                <img src="{{url('img/wa_logo_2.png')}}" class="share_bt_fb sbtfb_twitter" />
                            </div>
                        </div>

                        <!--  Mobile -->

                        <div class="hidden-md hidden-lg hidden-sm" data-toggle="sticky-onscroll">
                            <div class="share_bt_mb" onclick="share(1)">
                                <img src="{{url('img/facebook_logo.png')}}" class="share_bt_fb sbtfb_twitter"  style="position: relative;"/>
                                Share
                            </div>
                            <div class="share_bt sbt_twitter" onclick="share(2)">
                                <img src="{{url('img/twitter_icon.png')}}" class="share_bt_fb sbtfb_twitter"  />
                            </div>
                            <div class="share_bt_mb sbt_wa_mb" onclick="share(3)">
                                <img src="{{url('img/wa_logo_2.png')}}" class="share_bt_fb sbtfb_twitter" />
                            </div>
                        </div>
                    </center>
                    </p>

                    <p>

                    <center>
                        <a class="hidden-sm hidden-xs" href="#" onclick="retake_quiz('{{ url('dq/'.$result->quizz->slug) }}')">
                            <div class="share_bt retake_test_btn">
                                <img src="{{url('img/retake_test_logo.png')}}" class="share_bt_fb" />
                                Retake test
                            </div>
                        </a>
                        <a class="hidden-md hidden-lg" href="#" onclick="retake_quiz_mobile('{{ url('dq/'.$result->quizz->slug) }}')">
                            <div class="share_bt retake_test_btn">
                                <img src="{{url('img/retake_test_logo.png')}}" class="share_bt_fb" />
                                Retake test
                            </div>
                        </a>
                    </center>

                    </p>

                </div>
                <!-- End Div with id view_result_section_1 -->

                <!-- Start div with id view_result_section_2 -->
                <div class="text-center col-md-8 hidden-sm hidden-xs" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; display: none" id="view_result_section_2">

                    <p style="font-size: 40px; margin-top: 40px">

                        <img src="{{url('img/ripple4.svg')}}">
                    </p>
                    <h3 style="font-size: 32px">Analysing profile <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row col-md-8 col-md-offset-2" style="border: 1px solid #336699; border-radius: 5px;">
                        <div class="row">
                            <div class="col-md-12" style="background: #336699; padding: 15px; color:#fff; font-size: 20px"> Hit like to continue </div>
                        </div>

                        <div class="row" style="background:#fff; padding-bottom: 20px; border-radius: 5px">
                            <br />
                            <div class="col-md-2">
                                <img src="{{url('img/logo-navbar.png')}}" class="img img-responsive">
                            </div>
                            <div class="col-md-10">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false" data-width="300" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Mobile -->
                <div class="hidden-md hidden-lg text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; display: none" id="view_result_section_m_2">

                    <p style="font-size: 40px; margin-top: 5px">

                        <img src="{{url('img/ripple4.svg')}}" style="width:40%">
                    </p>
                    <h3 style="font-size: 25px">Analysing profile <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row" style="border: 1px solid #336699; border-radius: 5px; margin: 0 10px">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="background: #336699; padding: 0 5px; color:#fff; font-size: 20px"> Hit like to continue </div>

                        <br />
                        <div class="col-sm-12 col-xs-12" style="background: #fff; border-radius: 5px">
                            <div style="padding: 10px 0">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false" data-width="80"></div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- End Mobile -->

                <!-- End div with id view_result_section_2 -->

                <!-- Start div with id view_result_section_3 -->
                <div class="text-center col-md-8 hidden-sm hidden-xs" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; display: none" id="view_result_section_3">

                    <p style="font-size: 40px; margin-top: 40px">

                        <img src="{{url('img/ripple4.svg')}}">
                    </p>
                    <h3 style="font-size: 32px">Generating result <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row col-md-8 col-md-offset-2" style="border: 1px solid #336699; border-radius: 5px;">
                        <div class="row">
                            <div class="col-md-12" style="background: #336699; padding: 15px; color:#fff; font-size: 20px"> Hit like to continue </div>
                        </div>

                        <div class="row" style="background:#fff; padding-bottom: 20px; border-radius: 5px">
                            <br />
                            <div class="col-md-2">
                                <img src="{{url('img/logo-navbar.png')}}" class="img img-responsive">
                            </div>
                            <div class="col-md-10">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false" data-width="300" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Mobile -->
                <div class="hidden-md hidden-lg text-center" style="width: 100%; height: 540px; background-color: #cce4ff; border-radius: 5px; display: none" id="view_result_section_m_3">

                    <p style="font-size: 40px; margin-top: 5px">

                        <img src="{{url('img/ripple4.svg')}}" style="width:40%">
                    </p>
                    <h3 style="font-size: 25px">Generating result <span class="one">.</span><span class="two">.</span><span class="three">.</span></h3>
                    <br />
                    <div class="row" style="border: 1px solid #336699; border-radius: 5px; margin: 0 10px">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="background: #336699; padding: 0 5px; color:#fff; font-size: 20px"> Hit like to continue </div>

                        <br />
                        <div class="col-sm-12 col-xs-12" style="background: #fff; border-radius: 5px">
                            <div style="padding: 10px 0">
                                <div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false" data-width="80"></div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- End Mobile -->

                <!-- End div with id view_result_section_3 -->

            </div>
            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($quizzs->random(3) as $quizz)
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center>
                            <a class="text-center" id="quiz_link" href="{{ url('q/'.$quizz->slug) }}" onclick="show_loader('{{ $quizz->slug }}')">
                                <div class="text-center" style="padding: 5px; margin-bottom:0px; border-radius: 5px; border:0px solid #fff; width: 100%; height: 180px; background-image: url('{{ url('uploads/quizz/'.$quizz->image) }}'); background-size: cover; background-position: center center">
                                </div>
                                <p style="text-align: left">
                                    <span style="color:#000; font-size: 16px">{{ $quizz->translation->title }}</span>
                                </p>
                            </a>
                        </center>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-nude-gray">
        <div class="container">
            <div class="row items-row col-md-12">
                @include('front.pages.quizzs', ['tags' => $quizzs])
            </div>
        </div>
            <div class="container">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center">
                    <button id="btn_load_more" class="btn btn-primary" style="width: 90%; padding: 15px; font-size: 23px; font-weight: bold; margin-top: 10px; color: #fff;background-color: #3097D1;border-color: #2a88bd;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;
                        @lang('app.discover_more')
                    </button>
                </div>
            </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="texq/javascript"></script>
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="texq/javascript"></script>
@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function Share(url, slug){
            var w = 680;
            var h = 320;
            var left = (screen.width / 2) - (w / 2);
            var top = 150;
            var win = window.open(url, 'Partager', 'toolbar=no, status=no, menubar=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        }
        function show_loader_related(quiz_id){
            var html = '<i class="fa fa-refresh fa-spin fa-2x fa-fw"></i>';
            $('#btn_next_'+quiz_id).html(html);
        }

        $(document).ready(function() {

            var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
                var stickyHeight = sticky.outerHeight();
                var stickyTop = stickyWrapper.offset().top;
                if (scrollElement.scrollTop() >= stickyTop){
                    stickyWrapper.height(stickyHeight);
                    sticky.addClass("is-sticky");
                }
                else{
                    sticky.removeClass("is-sticky");
                    stickyWrapper.height('auto');
                }
            };

            $('[data-toggle="sticky-onscroll"]').each(function() {
                var sticky = $(this);
                var stickyWrapper = $('<div>').addClass('sticky-wrapper');
                sticky.before(stickyWrapper);
                sticky.addClass('sticky');


                $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
                    stickyToggle(sticky, stickyWrapper, $(this));
                });

                stickyToggle(sticky, stickyWrapper, $(window));
            });
        });

        function retake_quiz(login_url){
            $('#quiz_before_play_section').attr('onclick', '');
            $('#view_result_section_1').hide();
            $('#view_result_section_2').show();
            setTimeout( function(){
                $('#view_result_section_2').hide();
                $('#view_result_section_3').show();
            }  , 3000 );
            setTimeout( function(){
                window.location.href = login_url;
            }  , 4000 );
        }

        function retake_quiz_mobile(login_url){
            $('#quiz_before_play_section_m').attr('onclick', '');
            $('#view_result_section_1').hide();
            $('#view_result_section_m_2').show();
            setTimeout( function(){
                $('#view_result_section_m_2').hide();
                $('#view_result_section_m_3').show();
            }  , 3000 );
            setTimeout( function(){
                window.location.href = login_url;
            }  , 4000 );
        }
        function share(share_source){
            if(share_source == 1){
                FB.ui({
                    method: 'share',
                    href: '{{ url('qrs/'.$result->quizz->slug) }}',
                    display: 'popup',
                }, function(response){});
            }else if(share_source == 2){
                window.open('http://www.twitter.com/share?url={{ Request::url() }}');
            }else if(share_source == 3){
                window.open('whatsapp://send?text={{ Request::url() }}');
            }

            // Sav if shared

        }
    </script>
@endsection
@endsection