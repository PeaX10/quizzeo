<nav class="navbar navbar navbar-static-top" role="navigation-demo" id="navbar">
    <div class="container">

        <div class="navbar-header">
            <a href="{{ url('/') }}">
                <div class="logo-container" style="margin-left:5px">
                    <div class="logo">
                        <img src="{{ url('img/logo-navbar.png') }}" alt="Logo MEGATAG">
                    </div>
                    <div class="brand" style="padding-left: 20px;">
                        <div class="fb-like" data-href="@lang('app.facebook_link')" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false" data-width="80" style="width: 100%"></div>
                    </div>
                </div>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right" style="margin-right:5px;">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                    <img src="{{ url('img/flags/'.App::getLocale().'.png') }}">
                    <span class="hidden-sm hidden-xs">{{ config('settings.langs')[App::getLocale()] }}</span>
                    <span class="hidden-lg hidden-md">{{ strtoupper(@App::getLocale()) }}</span>
                    <b class="caret"></b>
                    <div class="ripple-container"></div></a>
                <ul class="dropdown-menu" style="background: #FFF; border: 1px solid #EEE; min-width:50px">
                    @foreach(config('settings.langs') as $key => $lang)
                        <li><a href="http://{{ $key }}.{{ env('APP_DOMAIN') }}"><img src="{{ url('img/flags/'.$key.'.png') }}"> <span class="hidden-md hidden-lg">{{strtoupper($key)}}</span><span class="hidden-xs hidden-sm">{{ $lang }}</span></a></li>
                    @endforeach
                    @if(count(config('settings.soon_langs')) > 0)
                        <li class="divider"></li>
                    @endif
                    @foreach(config('settings.soon_langs') as $key => $lang)
                        <li><a href="#"><img src="{{ url('img/flags/'.$key.'.png') }}"> <span class="hidden-md hidden-lg">{{strtoupper($key)}}</span><span class="hidden-xs hidden-sm">{{ $lang }} <span class="label label-default">@lang('app.menu_soon')</span></span></a></li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
</nav>
