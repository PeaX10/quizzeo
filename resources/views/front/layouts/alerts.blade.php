@if(Session::has('fb_login'))
    <div class="alert alert-success alert-with-icon" data-notify="container">
        <div class="container">
            <div class="alert-wrapper">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <i class="alert-icon fa fa-facebook"></i>
                <div class="message">@lang('app.alert_fb_login')</div>
            </div>
        </div>
    </div>
    <script src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#chooseFriend').trigger('click');
            $('#doQuizz').trigger('click');
        });
    </script>
@endif
@if(Session::has('sync_friends'))
    <div class="alert alert-success alert-with-icon" data-notify="container">
        <div class="container">
            <div class="alert-wrapper">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <i class="alert-icon fa fa-refresh"></i>
                <div class="message">@lang('app.alert_sync_friends')</div>
            </div>
        </div>
    </div>
    <script src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#chooseFriend').trigger('click');
            $('#doQuizz').trigger('click');
        });
    </script>
@endif
@if(Session::has('alert_error'))
    <div class="alert alert-danger alert-with-icon" data-notify="container">
        <div class="container">
            <div class="alert-wrapper">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <i class="alert-icon fa fa-exclamation-triangle"></i>
                <div class="message">@lang('app.alert_error')</div>
            </div>
        </div>
    </div>
@endif
@if(Session::has('account_deleted'))
    <div class="alert alert-success alert-with-icon" data-notify="container">
        <div class="container">
            <div class="alert-wrapper">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <i class="alert-icon fa fa-trash"></i>
                <div class="message">@lang('app.account_success_delete')</div>
            </div>
        </div>
    </div>
@endif
