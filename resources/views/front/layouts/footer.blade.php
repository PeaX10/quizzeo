<footer class="footer footer-transparent">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="logo text-center">
                    <h3><img src="{{ url('img/logo-footer.png') }}" style="max-width:70px"/></h3>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-2 col-sm-8">
                <div class="links">
                    <ul>
                        <li>
                            <a href="{{ url('terms') }}">
                                @lang('app.footer_terms')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('privacy') }}">
                                @lang('app.footer_privacy')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('legal') }}">
                                @lang('app.footer_legal')
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('delete') }}">
                                @lang('app.footer_delete')
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <div class="copyright">
                        <div class="pull-left">
                            © 2017 MEGATAG - Developed by <a target="_blank" href="http://www.peaxcreative.com">PeaX</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>