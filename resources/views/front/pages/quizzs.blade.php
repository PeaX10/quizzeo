@foreach($tags as $tag)
    <div class="col-md-4 col-sm-6 col-xs-12">
        <center>
            <a class="text-center" id="quiz_link" href="{{ url('t/'.$tag->slug) }}" onclick="show_loader('{{ $tag->slug }}')">
                <div class="text-center" style="padding: 5px; margin-bottom:8px; border-radius: 5px; border:15px solid #fff; width: 100%; height: 320px; background-image: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)),url('{{ url('uploads/tag/'.$tag->image) }}'); background-size: cover; background-position: center center;">

                    <h4 style="color:#fff; font-size: 28px; font-weight: 700; padding-top: 2%; text-decoration: none; height: 100px;">
                        {{ $tag->translation->title }}
                    </h4>
                    <p>
                        <button id="btn_next_{{ $tag->slug }}" class="btn btn-primary" style="position: relative; margin-top: 35px; width:85px; height:85px; border-radius: 50%; background-color: rgba(3,169,244, 0.7); font-size: 20px; font-weight: 700; border:none; color:#FFF">
                            @lang('app.quizz_next')
                        </button>
                    </p>
                    @if(!empty(Auth::user()) && Auth::user()->role == -1)
                    <div style="position: absolute; bottom:25px; left:30px; height:70px; width:100px; background-color:rgba(0,0,0,0.5)">
                        <span id="played"><i class="fa fa-play"></i> : {{ $played = \App\Stats::where('quizz', $tag->id)->where('type', 'play')->count() }}</span><br>
                        <span id="shared"><i class="fa fa-share"></i> : {{ $shared = \App\Stats::where('quizz', $tag->id)->where('type', 'share')->count() }}</span><br>
                        @if($played != 0)<span id="played"><i class="fa fa-percent"></i> : {{ round(($shared/$played)*100) }}%</span>@endif
                    </div>
                    @endif
                </div>
            </a>
        </center>
    </div>
@endforeach