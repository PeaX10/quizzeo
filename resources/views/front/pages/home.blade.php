@extends('front.layouts.default')

@section('title')
    MEGATAG - @lang('app.home_title')
@endsection

@section('content')
    @if(count($tags))
    <div class="main">
        <div class="section text-center landing-section section-nude-gray">
            <div class="container">
                <div class="row items-row quizzs">
                    @include('front.pages.quizzs')
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 text-center">
                        <button id="btn_load_more" class="btn btn-primary" style="width: 90%; padding: 15px; font-size: 23px; font-weight: bold; margin-top: 10px; color: #fff;background-color: #3097D1;border-color: #2a88bd;"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;
                            @lang('app.discover_more')
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
          <div class="section section-white-gray text-center">
              <h2>@lang('app.sorry')</h2>
              <h5><p>@lang('app.sorry_no_quizz')</p></h5>
          </div>
    @endif
@endsection

@section('js')
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).ready(function() {
                $('.items-row').infinitescroll({
                    loading: {
                        finishedMsg: "<br><div class='end-msg'>@lang('app.loading_no_quizz')</div>",
                        msgText: '<div class="col-md-4 col-md-offset-4 text-center"><div class="preloader"><div class="uil-reload-css" style=""><div></div></div><h5>@lang('app.loading')</h5></div></div></div>',
                        img: "{{ url('img/blank.png') }}"
                    },
                    navSelector: "ul.pager",
                    nextSelector: "ul.pager a[rel='next']",
                    itemSelector: ".item",
                    extraScrollPx: 50,
                    bufferPx     : 40,
                    errorCallback: function(){},
                });
            });
        });
    </script>
@endsection