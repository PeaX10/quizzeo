@extends('front.layouts.default')

@section('title')
    @lang('app.legal') - MEGATAG
@endsection

@section('content')
    <div class="section section-nude-gray">
        <div class="container">
            @lang('app.html_legal')
        </div>
    </div>
@endsection