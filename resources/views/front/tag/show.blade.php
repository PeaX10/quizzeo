@extends('front.layouts.default')

@section('title') {{ $pagetag->translation->title }} - TagYF @endsection

@section('meta')
    <meta property="fb:app_id" content="1251404328212801">
    <meta property="og:site_name" content="MEGATAG.NET">
    <meta property="og:type" content="website">
    <meta property="og:title"         content="{{ $pagetag->translation->title }}" />
    <meta property="og:description"   content="@lang('app.share_description')" />
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:image"         content="{{ url('uploads/result/'.$result->image) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $pagetag->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/result/'.$result->image) }}">
    <meta name="author" content="MEGATAG">
@endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container">
            <div class="col-md-8">
                <div class="card tag" data-background="color" data-color="white">
                    <div class="content text-center">
                        <h4 class="title text-primary">{{ $pagetag->translation->title }}</h4>
                        <img src="{{ url('uploads/result/'.$result->image) }}" alt="Tag : {{ $pagetag->translation->title }} - TagYF">
                        <hr>
                    </div>
                    <div class="card-footer" align="center">
                        @if(Auth::check())
                            <form action="{{ url('d/'.$pagetag->slug) }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="hidden-xs">
                                    <a class="btn btn-primary btn-fill btn-magnify" data-toggle="modal" data-target="#friendModal"><i class="fa fa-tags"></i> @lang('app.do_tag')</a>
                                    <button class="btn btn-info btn-fill btn-magnify"><i class="fa fa-star"></i> @lang('app.do_lucky_tag')</button>
                                </div>
                                <div class="hidden-sm hidden-md hidden-lg">
                                    <a class="btn btn-primary btn-fill btn-magnify btn-block" data-toggle="modal" data-target="#friendModal"><i class="fa fa-tags"></i> @lang('app.do_tag')</a>
                                    <button class="btn btn-info btn-fill btn-magnify btn-block"><i class="fa fa-star"></i> @lang('app.do_lucky_tag')</button>
                                </div>
                            </form>
                        @else
                            <!--
                            <a id="fbLogin" href="{{ url('auth/facebook') }}" class="btn btn-facebook btn-fill"><i class="fa fa-facebook"></i><span class="hidden-xs"> @lang('app.loading_fb')</span></a>
                            -->
                            <a class="btn btn-primary btn-fill btn-magnify" href="{{ url('auth/facebook') }}"><i class="fa fa-tags"></i> @lang('app.do_tag')</a>
                        @endif
                        <hr>
                        <a class="text-primary">@lang('app.tag_do_description')</a>
                        <!--
                        <a href="{{ url('t/'.$pagetag->slug) }}/luck" class="btn btn-info btn-fill btn-magnify"><i class="fa fa-star"></i><span class="hidden-xs"> @lang('app.do_lucky_tag')</span></a>
                        -->
                    </div>
                </div>
            </div>
            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($tags->random(2) as $tag)
                    <div class="col-md-12">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('t/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/tag/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('t/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    <!--
                                    <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                    <a href="{{ url('t/'.$tag->slug) }}/luck" class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></a>
                                    -->
                                    <form action="{{ url('d/'.$tag->slug) }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                        <button class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></button>
                                    </form>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row items-row col-md-12">
                @foreach($tags as $tag)
                    <div class="col-md-4 col-sm-6 item">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('t/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/tag/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('t/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    <!--
                                    <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>

                                    <a href="{{ url('t/'.$tag->slug) }}/luck" class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></a>
                                    -->
                                    <form action="{{ url('d/'.$tag->slug) }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                        <button class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></button>
                                    </form>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{ $tags->render() }}
    </div>
@endsection

@section('js')
    @if(Auth::check())
        <div class="modal fade" id="friendModal" tabindex="-1" role="dialog" aria-labelledby="friendModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form action="{{ url('d/'.$pagetag->slug) }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="friendModalLabel">@lang('app.choose_friend')</h4>
                        </div>
                        <div class="modal-body">
                            <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="friend" data-style="btn-info btn-block" data-menu-style="dropdown-blue">
                                @foreach($friends as $friend)
                                    <option value="{{ $friend->name }}" data-content="<img src='{{ $friend->picture }}' class='img-circle img-select' style='margin-right:10px' />{{ $friend->name }}">{{ $friend->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="data" id="data_friend">
                            <hr>
                            @lang('app.any_problems') <a id="syncFriend" href="{{ url('sync/friends') }}">@lang('app.sync_facebook_friends')</a>
                        </div>
                        <div class="modal-footer">
                            <div class="left-side">
                                <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">@lang('app.cancel')</button>
                            </div>
                            <div class="divider"></div>
                            <div class="right-side">
                                <input type="submit" class="btn btn-info btn-simple" value="@lang('app.confirm')">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).ready(function() {
                $('.items-row').infinitescroll({
                    loading: {
                        finishedMsg: "<br><div class='end-msg'>@lang('app.loading_no_tag')</div>",
                        msgText: '<div class="col-md-4 col-md-offset-4 text-center"><div class="preloader"><div class="uil-reload-css" style=""><div></div></div><h5>@lang('app.loading')</h5></div></div></div>',
                        img: "{{ url('img/blank.png') }}"
                    },
                    navSelector: "ul.pager",
                    nextSelector: "ul.pager a[rel='next']",
                    itemSelector: ".item",
                    extraScrollPx: 50,
                    bufferPx     : 40,
                    errorCallback: function(){},
                });
            });
        });
    </script>
@endsection