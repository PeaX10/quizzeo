@extends('front.layouts.default')

@section('meta')
    <meta property="fb:app_id" content="1251404328212801">
    <meta property="og:site_name" content="MEGATAG.NET">
    <meta property="og:type" content="website">
    <meta property="og:title"         content="{{ $result->tag->translation->title }}" />
    <meta property="og:description"   content="@lang('app.share_description')" />
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:image"         content="{{ url('uploads/result/'.$result->image) }}" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:locale" content="{{ App::getLocale() }}">
    <meta name="twitter:card" content="photo">
    <meta name="twitter:title" content="{{ $result->tag->translation->title }}">
    <meta name="twitter:image" content="{{ url('uploads/result/'.$result->image) }}">
    <meta name="author" content="{{ $result->user->name }}">
@endsection
@section('title') {{ $result->tag->translation->title }} - TagYF @endsection

@section('content')
    <div class="section section-white-gray">
        <div class="container">
            <div class="col-md-8">
                <div class="card tag" data-background="color" data-color="white">
                    <div class="content text-center">
                        <div class="hidden-xs">
                            <div class="row share">
                                <div class="col-md-10" align="center">
                                    <a class="btn btn-facebook btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('r/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> @lang('app.tag_share_fb')</a>
                                </div>
                                <div class="col-md-2" align="center">
                                    <a class="btn btn-twitter btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://twitter.com/intent/tweet?original_referer={{ Request::url() }}&text={{ $result->tag->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> @lang('app.tag_share_twitter')</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm">
                            <div class="row share">
                                <div class="col-xs-6" align="center">
                                    <a class="btn btn-facebook btn-fill btn-lg btn-icon btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('r/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> </a>
                                </div>
                                <div class="col-xs-6" align="center">
                                    <a class="btn btn-twitter btn-fill btn-lg btn-icon btn-magnify" href="#" onclick="Share('https://twitter.com/intent/tweet?original_referer={{ Request::url() }}&text={{ $result->tag->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> </span></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4 class="title text-primary">{{ $result->tag->translation->title }}</h4>
                        <img src="{{ url('uploads/result/'.$result->image) }}" alt="Tag : {{ $result->tag->translation->title }} - TagYF">
                        <br><br>
                        <div class="fb-like" data-href="https://www.facebook.com/TagYF-303395776670425/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
                        <hr>
                    </div>
                    <div class="card-footer" align="center">
                        <div class="hidden-xs">
                            <a href="{{ url('t/'.$result->tag->slug) }}" class="btn btn-block btn-lg btn-fill btn-primary btn-magnify"><i class="fa fa-tags"></i>   @lang('app.do_tag_other')</a>
                            <hr>
                            <a class="text-primary">@lang('app.tag_do_description')</a>
                            <div class="row share">
                                <hr>
                                <div class="col-md-10" align="center">
                                    <a class="btn btn-facebook btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('r/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> @lang('app.tag_share_fb')</a>
                                </div>
                                <div class="col-md-2" align="center">
                                    <a class="btn btn-twitter btn-fill btn-lg btn-block btn-magnify" href="#" onclick="Share('https://twitter.com/intent/tweet?original_referer={{ Request::url() }}&text={{ $result->tag->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> @lang('app.tag_share_twitter')</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm">
                            <a href="{{ url('t/'.$result->tag->slug) }}" class="btn btn-md btn-fill btn-block btn-primary btn-magnify"><i class="fa fa-tags"></i> @lang('app.do_tag_other')</a>
                            <div class="row share">
                                <hr>
                                <div class="col-xs-6" align="center">
                                    <a class="btn btn-facebook btn-fill btn-lg btn-icon btn-magnify" href="#" onclick="Share('https://www.facebook.com/sharer/sharer.php?u={{ url('r/'.$result->slug) }}', '{{ $result->slug }}')"><i class="fa fa-facebook-square"></i> </a>
                                </div>
                                <div class="col-xs-6" align="center">
                                    <a class="btn btn-twitter btn-fill btn-lg btn-icon btn-magnify" href="#" onclick="Share('https://twitter.com/intent/tweet?original_referer={{ Request::url() }}&text={{ $result->tag->translation->title }} {{ Request::url() }}', '{{ $result->slug }}')"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> </span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 items-row hidden-xs hidden-sm">
                @foreach($tags->random(3) as $tag)
                    <div class="col-md-12">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('t/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/tag/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('t/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    <!--
                                    <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                    <a href="{{ url('t/'.$tag->slug) }}/luck" class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></a>
                                    -->
                                    <form action="{{ url('d/'.$tag->slug) }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                        <button class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></button>
                                    </form>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section section-gray">
        <div class="container">
            <div class="row items-row col-md-12">
                @foreach($tags as $tag)
                    <div class="col-md-4 col-sm-6 item">
                        <div class="card card-hover-effect" data-background="color" data-color="nude-gray">
                            <a href="{{ url('t/'.$tag->slug) }}">
                                <div class="header header-with-icon" style="background: url('{{ url('uploads/tag/'.$tag->image) }}') no-repeat center / cover">
                                </div>
                                <div class="content text-center">
                                    <h6 class="title"><a href="{{ url('t/'.$tag->slug) }}">{{ $tag->translation->title }}</a></h6>
                                </div>
                                <div class="card-footer" align="center">
                                    <!--
                                    <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                    <a href="{{ url('t/'.$tag->slug) }}/luck" class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></a>
                                    -->
                                    <form action="{{ url('d/'.$tag->slug) }}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <a href="{{ url('t/'.$tag->slug) }}" class="btn btn-icon btn-fill btn-primary btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_tag')"><i class="fa fa-tags"></i></a>
                                        <button class="btn btn-icon btn-fill btn-info btn-tooltip" data-toggle="tooltip" data-placement="top" title="@lang('app.do_lucky_tag')"><i class="fa fa-star"></i></button>
                                    </form>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{ $tags->render() }}
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap3-typeahead.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('js/jquery.infinitescroll.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        function Share(url, slug){
            var w = 680;
            var h = 320;
            var left = (screen.width / 2) - (w / 2);
            var top = 150;
            var win = window.open(url, 'Partager', 'toolbar=no, status=no, menubar=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
        }
        $(document).ready(function(){
            $(document).ready(function() {
                $('.items-row').infinitescroll({
                    loading: {
                        finishedMsg: "<br><div class='end-msg'>@lang('app.loading_no_tag')</div>",
                        msgText: '<div class="col-md-4 col-md-offset-4 text-center"><div class="preloader"><div class="uil-reload-css" style=""><div></div></div><h5>@lang('app.loading')</h5></div></div></div>',
                        img: "{{ url('img/blank.png') }}"
                    },
                    navSelector: "ul.pager",
                    nextSelector: "ul.pager a[rel='next']",
                    itemSelector: ".item",
                    extraScrollPx: 50,
                    bufferPx     : 40,
                    errorCallback: function(){},
                });
            });
        });
    </script>
@endsection