<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ url('img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>@yield('title')- MEGATAG Back Office</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" />

    <link href="{{ url('css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>

    <link href="{{ url('css/demo.css') }}" rel="stylesheet" />

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ url('css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
    @yield('css')

</head>
<body>
<div class="wrapper">
    @include('back.layouts.sidebar')
    <div class="main-panel">
        @include('back.layouts.menu_top')
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>
<script src="{{ url('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/jquery.validate.min.js') }}"></script>
<script src="{{ url('js/moment.min.js') }}"></script>
<script src="{{ url('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ url('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{ url('js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>
<script src="{{ url('js/chartist.min.js') }}"></script>
<script src="{{ url('js/bootstrap-notify.js') }}"></script>
<script src="{{ url('js/sweetalert2.js') }}"></script>
<script src="{{ url('js/jquery-jvectormap.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="{{ url('js/jquery.bootstrap.wizard.min.js') }}"></script>
<script src="{{ url('js/bootstrap-table.js') }}"></script>
<script src="{{ url('js/fullcalendar.min.js') }}"></script>
<script src="{{ url('js/light-bootstrap-dashboard.js') }}"></script>
<script src="{{ url('js/demo.js') }}"></script>
@yield('js')
</html>
