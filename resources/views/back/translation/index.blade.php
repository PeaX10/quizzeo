@extends('back.layouts.default', ['active' => 'translation'])

@section('title') Traductions @endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <table id="bootstrap-table" class="table">
                <thead>
                <th data-field="id" data-sortable="true" class="text-center">ID</th>
                <th data-field="name" class="text-center" data-sortable="true">Nom</th>
                <th data-field="type" class="text-center" data-sortable="true">Type</th>
                <th data-field="visible" class="text-center" data-sortable="true">En-Ligne?</th>
                <th data-field="langs" class="text-center" data-sortable="false">Langues</th>
                <th data-field="created_at" class="hidden text-center" data-sortable="false">Date de création</th>
                <th data-field="updated_at" class="hidden text-center" data-sortable="false">Dernière màj</th>
                <th data-field="actions" class="text-right" data-events="operateEvents" data-formatter="operateFormatter">Actions</th>
                </thead>
                <tbody>
                @foreach($translations as $trans)
                    <?php
                      if(!empty($trans->quizz)){
                          $type = 'quizz';
                      }else{
                          $type = 'tag';
                      }
                    ?>
                    <tr>
                        <td>{{ $trans->id }}</td>
                        <td><b>
                            @if(!empty($trans->$type->translation->title))
                                {{ $trans->$type->translation->title }}
                            @elseif(!empty($trans->$type->slug))
                                {{ $trans->$type->slug }}
                            @endif
                            </b>
                        </td>
                        <td>{{ ucfirst($type) }}</td>
                        <td>@if($trans->visible)<span class="label label-success">Oui</span> @else <span class="label label-danger">Non</span>@endif</td>
                        <td>
                            @if($trans->lang != '')
                                @if($trans->visible)
                                    <span><img src="{{ url('img/flags/'.$trans->lang.'.png') }}"/></span>
                                @else
                                    <span><img class="filter-black img-rounded" src="{{ url('img/flags/'.$trans->lang.'.png') }}"/></span>
                                @endif
                            @endif
                        </td>
                        <td>{{ $trans->created_at->diffForHumans() }}</td>
                        <td>{{ $trans->updated_at->diffForHumans() }}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap-checkbox-radio-switch-quizzs.js') }}"></script>
    <script type="text/javascript">
        var $table = $('#bootstrap-table');

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Traduire" class="btn btn-simple btn-info btn-icon table-action edit" href="./translation/'+row.id+'/edit">',
                '<i class="fa fa-language"></i>',
                '</a>',
                '<a rel="tooltip" title="Supprimer" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)">',
                '<i class="fa fa-remove"></i>',
                '</a>'
            ].join('');
        }

        $().ready(function(){
            window.operateEvents = {
                'click .remove': function (e, value, row, index) {
                    // Ici on va en JSON envoyer l'ordre de la supprimer mais avec une confirmation avant
                    swal({  title: "Êtes-vous sûr ?",
                        html: 'Vous allez supprimer la traduction <b>'+row.name+' dans la langue '+row.lang+' et <span class="label label-info">ID: '+row.id+'</span></b><br>' +
                        'En appuyant sur le bouton "Confirmer", vous ne pourrez plus revenir en arrière !',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Confirmer",
                        cancelButtonText: "Annuler",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },function(isConfirm){
                        if (isConfirm){
                            $.ajax({
                                url: './translation/'+row.id,
                                type: 'delete',
                                data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                                success: function(result) {
                                    swal("Supprimé!", "La traduction a bien été supprimé.", "success");
                                    $table.bootstrapTable('remove', {
                                        field: 'id',
                                        values: [row.id]
                                    });
                                },
                                error: function(error){
                                    console.log(error);
                                    swal("Erreur!", "Une erreur c'est produite, la traduction n'a pas pu être supprimé.", "error");
                                }
                            });
                        }else{
                            swal("Annulé", "Ouff... La traduction est encore là :)", "error");
                        }
                    });
                }
            };

            $table.bootstrapTable({
                toolbar: ".toolbar",
                toolbarAlign: 'right',
                clickToSelect: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 15,
                clickToSelect: false,
                pageList: [15,30,50,100,200],

                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " traductions par page";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });

            //activate the tooltips after the data table is initialized
            $('[rel="tooltip"]').tooltip();

            $(window).resize(function () {
                $table.bootstrapTable('resetView');
            });

        });
        @if(Session::has('translation_updated'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre traduction a bien été modifié."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
    </script>
@endsection