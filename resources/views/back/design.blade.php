@extends('back.layouts.default', ['active' => 'design'])

@section('title') Designs @endsection

@section('content')
    <div class="col-md-12">
        <div class="card">

            <div class="toolbar">

            </div>

            <table id="bootstrap-table" class="table">
                <thead>
                <th data-field="id" class="text-center" data-sortable="true">ID</th>
                <th data-field="auteur" class="text-center" data-sortable="true">Auteur</th>
                <th data-field="type" class="text-center" data-sortable="true">Type</th>
                <th data-field="image" data-sortable="false" class="text-center hidden-sm hidden-xs">Proposition</th>
                <th data-field="actions" class="text-right" data-events="operateEvents" data-formatter="operateFormatter">Actions</th>
                </thead>
                <tbody>
                @foreach($designs as $design)
                    <tr>
                        <td>{{ $design->id }}</td>
                        <td>{{ $design->user->name }}</td>
                        @if($design->type == 'answer')
                            <td>Image de fond</td>
                            <td class="hidden-sm hidden-xs">
                                <div align="center">
                                    <img class="img-responsive img-rounded img-raised" src="{{ url('uploads/answer/preview/p_'.$design->design_id.'.jpg') }}" />
                                </div>
                            </td>
                        @elseif($design->type == 'quizz')
                            <td>Vignette et Image du Quizz</td>
                            <td class="hidden-sm hidden-xs">
                                <div align="center">
                                    <?php
                                        $image = json_decode($design->data)->image;
                                        $playImage = json_decode($design->data)->playImage;
                                    ?>
                                    <img class="img-responsive img-rounded img-raised" src="{{ url('uploads/quizz/'.$image) }}" />
                                    <img class="img-responsive img-rounded img-raised" src="{{ url('uploads/quizz/'.$playImage) }}" />
                                </div>
                            </td>
                        @endif
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap-checkbox-radio-switch-quizzs.js') }}"></script>
    <script type="text/javascript">
        var $table = $('#bootstrap-table');

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Approuver" class="btn btn-simple btn-success btn-icon table-action valid" href="./design/'+row.id+'/valid">',
                '<i class="fa fa-check"></i>',
                '</a>',
                '</a>',
                '<a rel="tooltip" title="Rejeter" class="btn btn-simple btn-danger btn-icon table-action valid" href="./design/'+row.id+'/delete">',
                '<i class="fa fa-times"></i>',
                '</a>'
            ].join('');
        }

        $().ready(function(){

            $table.bootstrapTable({
                toolbar: ".toolbar",
                toolbarAlign: 'right',
                clickToSelect: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 15,
                clickToSelect: false,
                pageList: [15,30,50,100,200],

                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " quizzs par page";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });

            //activate the tooltips after the data table is initialized
            $('[rel="tooltip"]').tooltip();

            $(window).resize(function () {
                $table.bootstrapTable('resetView');
            });

        });
        @if(Session::has('deleted'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - ce design a bien été supprimé."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
        @if(Session::has('deleted'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - ce design a bien été approuvé."

        },{
            type: 'success',
            timer: 4000
        });
        @endif

    </script>
@endsection