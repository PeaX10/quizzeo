@extends('back.layouts.default', ['active' => 'quizz'])

@section('title') Quizzs @endsection

@section('content')
    <div class="col-md-12">
        <div class="card">

            <div class="toolbar">
                <a href="{{ url('quizz/create') }}" id="add_quizz" class="btn btn-danger btn-round btn-fill"><i class="fa fa-plus"></i></a>
            </div>

            <table id="bootstrap-table" class="table">
                <thead>
                <th data-field="id" data-sortable="true" class="text-center">ID</th>
                <th data-field="slug" class="text-center" data-sortable="true">Nom</th>
                <th data-field="image" data-sortable="false" class="text-center hidden-sm hidden-xs">Image</th>
                <th data-field="visible" class="text-center" data-sortable="true">En-Ligne?</th>
                <th data-field="sexes" class="text-center" data-sortable="true">Sexes</th>
                <th data-field="langs" class="text-center" data-sortable="false">Langues</th>
                <th data-field="stats" class="text-center" data-sortable="false">Stats</th>
                <th data-field="created_at" class="hidden text-center" data-sortable="false">Date de création</th>
                <th data-field="updated_at" class="hidden text-center" data-sortable="false">Dernière màj</th>
                <th data-field="actions" class="text-right" data-events="operateEvents" data-formatter="operateFormatter">Actions</th>
                </thead>
                <tbody>
                @foreach($quizzs as $quizz)
                    <tr>
                        <td>{{ $quizz->id }}</td>
                        <td><b>
                            @if(!empty($quizz->translation->title))
                                {{ $quizz->translation->title }}
                            @else
                                {{ $quizz->slug }}
                            @endif
                            </b>
                        </td>
                        <td class="hidden-sm hidden-xs"><div align="center">@if(!empty($quizz->image))<img class="img-responsive img-rounded img-raised" style="max-width:100px" src="{{ url('uploads/quizz/'.$quizz->image) }}" />@endif</div></td>
                        <td>@if($quizz->visible)<span class="label label-success">Oui</span> @else <span class="label label-danger">Non</span>@endif</td>
                        <td>
                            @if($quizz->gender == 'male')
                                <i class="fa fa-mars"></i>
                            @elseif($quizz->gender == 'female')
                                <i class="fa fa-venus"></i>
                            @else
                                <i class="fa fa-venus-mars"></i>
                            @endif
                        </td>
                        <td>
                            @foreach(explode(',', $quizz->langs) as $lang)
                                @if($lang != '')
                                    <?php
                                        $s_lang = $lang;
                                        $lang = $quizz->translations()->where('lang', $lang)->first();
                                    ?>
                                    @if($lang != null && $lang->visible)
                                        <span><img src="{{ url('img/flags/'.$s_lang.'.png') }}"/></span>
                                    @else
                                        <span><img class="filter-black img-rounded" src="{{ url('img/flags/'.$s_lang.'.png') }}"/></span>
                                    @endif
                                @endif
                            @endforeach
                        </td>
                        <td>
                            <span id="played"><i class="fa fa-play"></i> : {{ $played = \App\Stats::where('quizz', $quizz->id)->where('type', 'play')->count() }}</span><br>
                            <span id="shared"><i class="fa fa-share"></i> : {{ $shared = \App\Stats::where('quizz', $quizz->id)->where('type', 'share')->count() }}</span><br>
                            @if($played != 0) <span id="played"><i class="fa fa-percent"></i> : {{ ($shared/$played)*100 }}%</span>@endif
                        </td>
                        <td>{{ $quizz->created_at->diffForHumans() }}</td>
                        <td>{{ $quizz->updated_at->diffForHumans() }}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/bootstrap-checkbox-radio-switch-quizzs.js') }}"></script>
    <script type="text/javascript">
        var $table = $('#bootstrap-table');

        function operateFormatter(value, row, index) {
            return [
                '<a rel="tooltip" title="Voir" class="btn btn-simple btn-info btn-icon table-action view" href="javascript:void(0)">',
                '<i class="fa fa-image"></i>',
                '</a>',
                '<a rel="tooltip" title="Modifier" class="btn btn-simple btn-warning btn-icon table-action edit" href="./quizz/'+row.id+'/edit">',
                '<i class="fa fa-edit"></i>',
                '</a>',
                '<a rel="tooltip" title="Supprimer" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)">',
                '<i class="fa fa-remove"></i>',
                '</a>'
            ].join('');
        }

        $().ready(function(){
            window.operateEvents = {
                'click .view': function (e, value, row, index) {
                    content =   '<h6>'+row.slug+'<span class="label label-info">ID: '+row.id+'</span></h6>' +
                                '<br>' +
                                row.image+'<br>' +
                                '<span>En ligne ?: </span>' +
                                row.visible+'<br>'+
                                '<span>Langues: </span>' +
                                row.langs+ '<br>' +
                                '<span>Sexes: </span>' +
                                row.sexes+ '<br>' +
                                '<span>Date de création: </span>' + row.created_at + '<br>' +
                                '<span>Dernière édition: </span>' + row.updated_at;
                    swal({
                        title: 'Information du quizz',
                        html: content,
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: true
                    });

                    console.log(info);
                },
                'click .remove': function (e, value, row, index) {
                    // Ici on va en JSON envoyer l'ordre de la supprimer mais avec une confirmation avant
                    swal({  title: "Êtes-vous sûr ?",
                        html: 'Vous allez supprimer le quizz <b>'+row.slug+'<span class="label label-info">ID: '+row.id+'</span></b><br>' +
                        'En appuyant sur le bouton "Confirmer", Toutes les données issues du quizz seront supprimées ainsi que des ses résultats, traductions, visites, stats...',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Confirmer",
                        cancelButtonText: "Annuler",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },function(isConfirm){
                        if (isConfirm){
                            $.ajax({
                                url: './quizz/'+row.id,
                                type: 'delete',
                                data: {_method: 'delete', _token : '{{ csrf_token() }}' },
                                success: function(result) {
                                    swal("Supprimé!", "Le quizz a bien été supprimé.", "success");
                                    $table.bootstrapTable('remove', {
                                        field: 'id',
                                        values: [row.id]
                                    });
                                },
                                error: function(error){
                                    console.log(error);
                                    swal("Erreur!", "Une erreur c'est produite, le quizz n'a pas pu être supprimé.", "error");
                                }
                            });
                        }else{
                            swal("Annulé", "Ouff... Le quizz est encore là :)", "error");
                        }
                    });
                }
            };

            $table.bootstrapTable({
                toolbar: ".toolbar",
                toolbarAlign: 'right',
                clickToSelect: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 15,
                clickToSelect: false,
                pageList: [15,30,50,100,200],

                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " quizzs par page";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });

            //activate the tooltips after the data table is initialized
            $('[rel="tooltip"]').tooltip();

            $(window).resize(function () {
                $table.bootstrapTable('resetView');
            });

        });
        @if(Session::has('added_quizz'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre quizz a bien été créer."

        },{
            type: 'success',
            timer: 4000
        });
        @endif

    </script>
@endsection