@extends('back.layouts.default', ['active' => 'quizz'])

@section('title')
    Paramètres du module
@endsection

@section('css')
    <link href="{{ url('css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <h4>Paramètres du module</h4>
        <div class="card">
            <div class="content">
                {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->action( url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module/'.$module_id.'/setting') )->enctype("multipart/form-data") !!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <hr>
                @endif
                <h4>Contenu</h4>
                <hr>
                @if(!empty($module->data))
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                @foreach($module->schema as $element)
                                    <th>{{ $element->name }}</th>
                                @endforeach
                                    <th>Sexe</th>
                                    <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($module->data as $key => $element)
                                <tr class="tr-module">
                                    @foreach($element as $key2 => $value)
                                            @if(substr($value, -4) == '.png')
                                                <td><img src="{{ url('uploads/module/'.$value) }}" class="img-responsive" /></td>
                                            @elseif($key2 != 'params_gender')
                                                <td><small>{{ $value }}</small></td>
                                            @endif
                                    @endforeach
                                        <td>
                                            @if($element->params_gender == 'male')
                                                <i class="fa fa-mars"></i>
                                            @elseif($element->params_gender == 'female')
                                                <i class="fa fa-venus"></i>
                                            @else
                                                <i class="fa fa-venus-mars"></i>
                                            @endif
                                        </td>
                                        <td class="td-actions">
                                            <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module/'.$module_id.'/setting/'.$key.'/edit') }}" type="button" rel="tooltip" data-placement="left" title="Modifier" class="btn btn-success btn-simple btn-icon">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id.'/module/'.$module_id.'/setting/'.$key.'/delete') }}" type="button" rel="tooltip" data-placement="left" title="Supprimer" class="btn btn-danger btn-simple btn-icon ">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <h4 align="center" class="text-danger">Votre module est pour le moment vide !</h4>
                @endif
                <br>
                <h4>Ajouter du contenu</h4>
                <hr>
                @foreach($module->schema as $key => $value)
                    @if($value->type == 'picture')
                        <div class="form-group">
                            <label class="col-sm-4 col-lg-3 control-label" for="visible">{{ $value->name }}</label>
                            <div class="col-sm-8 col-lg-9">
                                <label><small>Transférer une image</small></label>
                                <input 	type='file'
                                          class='input-ghost'
                                          name='image_{{ $value->name }}'
                                          style='visibility:hidden; height:0'
                                          onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                                <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                    <input 	type="text"
                                              class="form-control"
                                              placeholder='Choisissez une image...'
                                              style="cursor:pointer"
                                              onclick="$(this).parents('.input-file').prev().click(); return false;"
                                    />
                                </div>
                            </div>
                        </div>
                    @else
                        {!! BootForm::text($value->name, 'text_'.$value->name)->type('text')->placeholder('Entrer du texte')->value(old('text_'.$value->name) ? old('text_'.$value->name) : '') !!}
                    @endif
                @endforeach
                <div class="form-group">
                    <label class="col-md-3 control-label">Sexe</label>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" @if(empty(old('params_gender')) || old('params_gender') == 'all') checked @endif data-toggle="radio" name="params_gender" value="all"> <i class="fa fa-venus-mars"></i>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" @if(old('params_gender') == 'male') checked @endif data-toggle="radio" name="params_gender" value="male"> <i class="fa fa-mars"></i>
                            </label>
                        </div>
                        <div class="col-md-4">
                            <label class="radio">
                                <input type="radio" @if(old('params_gender') == 'female') checked @endif data-toggle="radio" name="params_gender" value="female"> <i class="fa fa-venus"></i>
                            </label>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <a href="{{ url('quizz/'.$answer->quizz_id.'/answer/'.$answer->id) }}" class="btn btn-rounded btn-fill pull-leftt">Retour</a>
                <input type="submit" class="btn btn-rounded btn-fill btn-info pull-right" value="Ajouter">
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('js/bootstrap-colorpicker.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            if($('.type .radio input:checked').val() == 'multivar'){
                $('.content-type.percent').addClass('hidden');
                $('.content-type.multivar').removeClass('hidden');
            }else if($('.type .radio input:checked').val() == 'percent'){
                $('.content-type.multivar').addClass('hidden');
                $('.content-type.percent').removeClass('hidden');
            }
            $(".type .radio input[type=radio]" ).on( "change", function(){
                if($('.type .radio input:checked').val() == 'multivar'){
                    $('.content-type.percent').addClass('hidden');
                    $('.content-type.multivar').removeClass('hidden');
                }else if($('.type .radio input:checked').val() == 'percent'){
                    $('.content-type.multivar').addClass('hidden');
                    $('.content-type.percent').removeClass('hidden');
                }
            });
            $('#cp1').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp2').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
            $('#cp3').colorpicker({
                customClass: 'colorpicker-2x',
                format: "hex"
            });
        });

        function addVar(variable){
            input = $('input[name="object"]');
            input.val(input.val()+variable);
        }

        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper         = $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed

                    $("#rm").remove();

                    $(wrapper).append('<div id="divs' + x + '">' +
                            '<div class="form-group">' +
                            '<label class="col-sm-4 col-lg-3 control-label" for="part">Nom de l\'élèment</label>' +
                            '<div class="col-sm-8 col-lg-9">' +
                            '<input type="text" name="elements_'+ x +'" id="elements" class="form-control" placeholder="Nom de l\'élèment">'+
                            '</div>'+
                            '<div class="form-group type">'+
                            '<label class="col-sm-4 col-lg-3 control-label">Type</label>'+
                            '<div class="col-sm-8 col-lg-9">'+
                            '<div class="col-md-4">'+
                            '<label class="radio checked">'+
                            '<span class="icons"><span class="first-icon fa fa-circle-o"></span><span class="second-icon fa fa-dot-circle-o"></span></span><input type="radio" data-toggle="radio" name="types_'+ x +'" value="picture"> <i class="fa fa-picture-o"></i>'+
                            '</label>'+
                            '</div>'+
                            '<div class="col-md-4">'+
                            '<label class="radio">'+
                            '<span class="icons"><span class="first-icon fa fa-circle-o"></span><span class="second-icon fa fa-dot-circle-o"></span></span><input type="radio" data-toggle="radio" name="types_'+ x +'" value="text"> <i class="fa fa-font"></i>'+
                            '</label> ' +
                            '</div> ' +
                            '</div><hr>' +
                            '</div></div>'
                    );
                    $(wrapper).append('<center><a href="#" id="rm" class="btn btn-fill btn-danger remove_field">Supprimer</a></center></div>'); //add input box
                    x++;
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault();
                x--;
                $("#divs"+ x).remove();

            })

            @if(Session::has('added_data'))
            $.notify({
                icon: 'pe-7s-bell',
                message: "<b>Félicitation</b> - votre contenu a bien été ajouté."

            },{
                type: 'success',
                timer: 4000
            });
            @endif
            @if(Session::has('updated_data'))
            $.notify({
                icon: 'pe-7s-bell',
                message: "<b>Félicitation</b> - votre contenu a bien été modifié."

            },{
                type: 'success',
                timer: 4000
            });
            @endif
            @if(Session::has('deleted_data'))
            $.notify({
                icon: 'pe-7s-bell',
                message: "<b>Félicitation</b> - votre contenu a bien été supprimé."

            },{
                type: 'success',
                timer: 4000
            });
            @endif
        });
    </script>
@endsection