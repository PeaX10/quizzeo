@extends('back.layouts.default', ['active' => 'quizz'])

@section('title') Ajouter une réponse @endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card card-wizard" id="wizardCard">
                    <form id="wizardForm" method="POST" action="{{ url('quizz/'.$quizz_id.'/answer') }}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="header text-center">
                            <h3 class="title">Answer Creator</h3>
                            <p class="category">Créer une réponse en toute simplicité</p>
                        </div>

                        <div class="content">
                            <ul class="nav">
                                <li><a href="#tab1" data-toggle="tab">Informations</a></li>
                                <li><a href="#tab2" data-toggle="tab">Composants</a></li>
                                <li><a href="#tab3" data-toggle="tab">Image</a></li>
                            </ul>
                            <div class="tab-content">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="tab-pane" id="tab1">
                                    <h5 class="text-center">Informations de bases sur la réponse.</h5>
                                    <div class="row">
                                        <div class="form-horizontal" style="padding:30px">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">En Ligne?</label>
                                                <div class="col-md-9">
                                                    <input type="checkbox" name="visible" @if(!empty(old('visible'))) checked @endif data-toggle="switch"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Sexe</label>
                                                <div class="col-md-9">
                                                    <div class="col-md-4">
                                                        <label class="radio">
                                                            <input type="radio" @if(empty(old('gender')) || old('gender') == 'all') checked @endif data-toggle="radio" name="gender" value="all"> <i class="fa fa-venus-mars"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="radio">
                                                            <input type="radio" @if(old('gender') == 'male') checked @endif data-toggle="radio" name="gender" value="male"> <i class="fa fa-mars"></i>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="radio">
                                                            <input type="radio" @if(old('gender') == 'female') checked @endif data-toggle="radio" name="gender" value="female"> <i class="fa fa-venus"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane" id="tab2">
                                    <h5 class="text-center">Quelles composants apparaitrons sur cette réponse ?</h5>
                                    <div class="row">
                                        <div class="form-horizontal" style="padding:30px">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nombre de texte</label>
                                                <div class="col-md-9">
                                                    <input type="number" min="0" max="10" class="form-control" value="@if(old('nb_text')) {{ old('nb_text') }} @else 0 @endif" name="nb_text"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Nombre d'image</label>
                                                <div class="col-md-9">
                                                    <input type="number" min="0" max="10" class="form-control" value="@if(old('nb_img')) {{ old('nb_img') }} @else 0 @endif" name="nb_img"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab3">
                                    <h5 class="text-center">Et la petite photo pour rendre le résultat tout beau !</h5>
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group">
                                                <label>Taille stricte de 1200x630 (8MB MAX)</label>
                                                <input 	type='file'
                                                          class='input-ghost'
                                                          name='image'
                                                          style='visibility:hidden; height:0'
                                                          onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                                                <div class="input-group input-file" name="Fichier_1">
                                                    <span class="input-group-btn">
                                                        <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                                   type="button"
                                                                   onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                                    </span>
                                                    <input 	type="text"
                                                              class="form-control"
                                                              placeholder='Choisissez un fichier...'
                                                              style="cursor:pointer"
                                                              onclick="$(this).parents('.input-file').prev().click(); return false;"
                                                    />
                                                    <span class="input-group-btn">
                                                         <button 	class="btn btn-danger btn-fill btn-reset"
                                                                    type="button"
                                                                    onclick="
                                                                        $(this).parents('.input-file').prev().val(null);
                                                                        $(this).parents('.input-file').find('input').val('');
                                                                    ">Effacer</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="footer">
                            <button type="button" class="btn btn-default btn-fill btn-wd btn-back pull-left">Précédent</button>

                            <button type="button" class="btn btn-info btn-fill btn-wd btn-next pull-right">Suivant</button>
                            <input type="submit" class="btn btn-info btn-fill btn-wd btn-finish pull-right" value="Valider">

                            <div class="clearfix"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $().ready(function(){

            // you can also use the nav-pills-[blue | azure | green | orange | red] for a different color of wizard

            $('#wizardCard').bootstrapWizard({
                tabClass: 'nav nav-pills',
                nextSelector: '.btn-next',
                previousSelector: '.btn-back',
                onNext: function(tab, navigation, index) {
                    var $valid = $('#wizardForm').valid();

                    if(!$valid) {
                        $validator.focusInvalid();
                        return false;
                    }
                },
                onInit : function(tab, navigation, index){

                    //check number of tabs and fill the entire row
                    var $total = navigation.find('li').length;
                    $width = 100/$total;

                    $display_width = $(document).width();

                    if($display_width < 600 && $total > 3){
                        $width = 50;
                    }

                    navigation.find('li').css('width',$width + '%');
                },
                onTabClick : function(tab, navigation, index){
                    // Disable the posibility to click on tabs
                    return false;
                },
                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;

                    var wizard = navigation.closest('.card-wizard');

                    // If it's the last tab then hide the last button and show the finish instead
                    if($current >= $total) {
                        $(wizard).find('.btn-next').hide();
                        $(wizard).find('.btn-finish').show();
                    } else if($current == 1){
                        $(wizard).find('.btn-back').hide();
                    } else {
                        $(wizard).find('.btn-back').show();
                        $(wizard).find('.btn-next').show();
                        $(wizard).find('.btn-finish').hide();
                    }
                }
            });

        });

        function onFinishWizard(){
            //here you can do something, sent the form to server via ajax and show a success message with swal

            swal("Good job!", "You clicked the finish button!", "success");
        }
    </script>
@endsection