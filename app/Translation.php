<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table = 'translations';

    protected $fillable = ['id', 'tag_id', 'lang', 'visible', 'data'];

    public function tag(){
        return $this->belongsTo('App\Tag');
    }

    public function quizz(){
        return $this->belongsTo('App\Quizz');
    }
}
