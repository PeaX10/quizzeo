<?php

namespace App\Http\Controllers;

use App\Translation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Quizz;
use App\Result;
use Illuminate\Support\Facades\App;
use App\Visit;
use phpDocumentor\Reflection\Types\This;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use App\User;
use App\I18N\Arabic as ArabicI18N;
use App\Stats;


class QuizzController extends Controller
{
    public function index(Request $request, $slug, LaravelFacebookSdk $fb){
        if(!empty($request->session()->get('fb_url'))){
            $quizz_url = $request->session()->pull('fb_url');
            $slug2 = explode('/q/', $quizz_url);
            if(!empty($slug2[1])) return redirect('/dq/'.$slug2[1]);
        }
        $pagequizz = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->where('quizzs.slug', $slug)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->firstOrFail();

        $quizzs = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.slug', '!=', $slug)
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->simplePaginate(18);
        if(Auth::check()) {
            $user = Auth::user();
            $friends = json_decode($user->friends);
        }

        if ($request->ajax()) {
            return Response::json(View::make('front.pages.quizzs', array('tags' => $quizzs))->render());
        }

        $this->updateVisit($request, $pagequizz->id);
        return view('front.quizz.show', compact('pagequizz', 'quizzs', 'friends'));
    }
    

    public function result(Request $request, $slug){
        $result = Result::where('slug', $slug)->firstOrFail();
        $quizzs = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.slug', '!=', $slug)
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->simplePaginate(18);
        if ($request->ajax()) {
            return Response::json(View::make('front.pages.quizzs', array('tags' => $quizzs))->render());
        }

        return view('front.quizz.result', compact('result', 'quizzs'));
    }

    public function result2(Request $request, $slug){
        $result = Result::where('slug', $slug)->firstOrFail();
        $quizzs = Quizz::join('translations', 'translations.quizz_id', '=', 'quizzs.id')
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->with('translations')
            ->simplePaginate(18);

        $crawlers = [
            'facebookexternalhit/1.1',
            'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)',
            'Facebot',
            'Twitterbot',
        ];

        $userAgent = $request->header('User-Agent');

        if (str_contains($userAgent, $crawlers)) {
            return view('front.quizz.resultshare', compact('result', 'quizzs'));
        }
        return redirect('/q/'.$result->quizz->slug);

    }

    public function updateVisit(Request $request, $quizz_id){
        // On vérifie si il n'y a pas eu de visite par le même utilisateur dans les 2 dernières minutes
        $nb_visits = Visit::where('ip', $request->ip())->where('quizz_id', $quizz_id)->where('created_at', '>=', Carbon::now()->subMinute(2))->count();
        if($nb_visits === 0){
            $visit = new Visit();
            if(Auth::check()) $visit->user_id = Auth::user()->id; else $visit->user_id = 0;
            $visit->quizz_id = $quizz_id;
            $visit->lang = App::getLocale();
            $visit->ip = $request->ip();
            $visit->previous_url = redirect()->back()->getTargetUrl();
            $visit->save();
        }
    }

    public function doQuizz(Request $request, $slug, LaravelFacebookSdk $fb){
        if(!$this->checkPermissions($fb)) return redirect($fb->getReRequestUrl(['email', 'user_friends']));
        $friends = $this->getFriends($fb);
        $myGender = Auth::user()->gender;
        if($myGender != 'male' && $myGender != 'female') $myGender = 'male';
        if($myGender == 'male') $opposite_gender = 'female'; else $opposite_gender = 'male';
        $men = [];
        $women = [];
        foreach($friends as $key => $value){
            if($this->getGender($value['first_name']) == 'male'){
                $men[] = $value;
            }else{
                $women[] = $value;
            }
        }
        // Random Friends
        if(count($friends) <= 20){
            $limit = count($friends);
        }else{
            $limit = 20;
        }
        if($limit > 0) $rfriends = array_rand($friends, $limit); else $rfriends = [];

        // Male Friends
        if(count($men) <= 20){
            $limit = count($men);
        }else{
            $limit = 20;
        }
        if($limit > 0) $mfriends = array_rand($men, $limit); else $mfriends = [];

        // Female Friends
        if(count($women) <= 20){
            $limit = count($women);
        }else{
            $limit = 20;
        }
        if($limit > 0) $ffriends = array_rand($women, $limit); else $ffriends = [];
        $answer = Quizz::where('slug', $slug)->firstOrFail()->answers()->where('visible', true)->get()->random(1);
        $result = $this->makeResult($answer, $friends, $friends, $rfriends, $mfriends, $ffriends, $men, $women);
        $stats = new Stats();
        $stats->type = 'play';
        $stats->user = Auth::user()->id;
        $stats->quizz = Quizz::where('slug', $slug)->firstOrFail()->id;
        $stats->ip = request()->ip();
        $stats->save();
        return redirect('qr/'.$result->slug);
    }

    public function getFriends(LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,first_name,last_name,gender,picture.type(square).width(1200).height(1200)&limit=150');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return back()->with('error_fb', true);
        }
        $friends = array();
        $end = true;
        $fb_friends = $response->getGraphEdge();
        foreach($fb_friends as $friend) {
            $data = [
                'id' => $friend->getField('id'),
                'name' => $friend->getField('name'),
                'first_name' => $friend->getField('first_name'),
                'last_name' => $friend->getField('last_name'),
                'gender' => $friend->getField('gender'),
                'picture' => $friend->getField('picture')->getField('url')
            ];
            array_push($friends, $data);
        }
        return $friends;
    }

    public function getFriend($fb_name, LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);

        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,first_name,last_name,gender,picture.type(square).width(1200).height(1200)&limit=150');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return back()->with('error_fb', true);
        }
        $end = true;
        $fb_friends = $response->getGraphEdge();
        while($end){
            foreach($fb_friends as $dfriend) {
                if($dfriend->getField('name') == $fb_name){
                    $friend = [
                        'id' => $dfriend->getField('id'),
                        'name' => $dfriend->getField('name'),
                        'first_name' => $dfriend->getField('first_name'),
                        'last_name' => $dfriend->getField('last_name'),
                        'gender' => $dfriend->getField('gender'),
                        'picture' => $dfriend->getField('picture')->getField('url')
                    ];
                    $end = false;
                }
            }
            if(isset($fb_friends->getMetaData('paging')['paging']['next'])){
                $after = $fb_friends->getMetaData('paging')['paging']['cursors']['after'];
                try {
                    $response = $fb->get('/me/taggable_friends?fields=id,name,picture.type(square).width(1200).height(1200),first_name,last_name,gender&after='.$after);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    return back()->with('error_fb', true);
                }
                $fb_friends = $response->getGraphEdge();
            }else{
                $end = false;
            }
        }
        if(empty($friend)){
            return back()->with('error_fb');
        }else{
            return $friend;
        }
    }

    public function randomFriend(LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);

        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,first_name,last_name,gender,picture.type(square).width(1200).height(1200)&limit=100');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return back()->with('error_fb', true);
        }
        $fb_friends = $response->getGraphEdge();
        $dfriend = $fb_friends[rand(0, count($fb_friends) - 1)];
        $friend = [
            'id' => $dfriend->getField('id'),
            'name' => $dfriend->getField('name'),
            'first_name' => $dfriend->getField('first_name'),
            'last_name' => $dfriend->getField('last_name'),
            'gender' => $dfriend->getField('gender'),
            'picture' => $dfriend->getField('picture')->getField('url')
        ];

        if(empty($friend)){
            return back()->with('error_fb');
        }else{
            return $friend;
        }
    }

    public function completeText($text, $module_text, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic, $answer){
        if(Auth::check()) $user = Auth::user();
        $user['name'] = $user->name;
        $user['first_name'] = $user->first_name;
        $user['last_name'] = $user->last_name;

        $ar = [
            '{! me.name !}' => $this->transliterateString($user['name']),
            '{! me.first_name !}' => $this->transliterateString($user['first_name']),
            '{! me.last_name !}' => $this->transliterateString($user['last_name']),
            '{! ME.NAME !}' => strtoupper($this->transliterateString($user['name'])),
            '{! ME.FIRST_NAME !}' => strtoupper($this->transliterateString($user['first_name'])),
            '{! ME.LAST_NAME !}' => strtoupper($this->transliterateString($user['last_name'])),
        ];
        $user = Auth::user();
        if($user->gender == 'female') $opposite_gender = 'male'; else $opposite_gender = 'female';
        $ar = [
            '{! me.name !}' => $this->transliterateString($user->name),
            '{! me.first_name !}' => $this->transliterateString($user->first_name),
            '{! me.last_name !}' => $this->transliterateString($user->last_name),
            '{! ME.NAME !}' => strtoupper($this->transliterateString($user->name)),
            '{! ME.FIRST_NAME !}' => strtoupper($this->transliterateString($user->first_name)),
            '{! ME.LAST_NAME !}' => strtoupper($this->transliterateString($user->last_name)),
        ];
        // Random Friends
        foreach($rfriends as $key => $value){
            $ar['{! friend'.($key + 1).'.name !}'] = $this->transliterateString($friends[$value]['name']);
            $ar['{! friend'.($key + 1).'.first_name !}'] = $this->transliterateString($friends[$value]['first_name']);
            $ar['{! friend'.($key + 1).'.last_name !}'] = $this->transliterateString($friends[$value]['last_name']);
            $ar['{! FRIEND'.($key + 1).'.NAME !}'] = strtoupper($this->transliterateString($friends[$value]['name']));
            $ar['{! FRIEND'.($key + 1).'.FIRST_NAME !}'] = strtoupper($this->transliterateString($friends[$value]['first_name']));
            $ar['{! FRIEND'.($key + 1).'.LAST_NAME !}'] = strtoupper($this->transliterateString($friends[$value]['last_name']));
        }
        foreach($mfriends as $key => $value){
            $ar['{! mfriend'.($key + 1).'.name !}'] = $this->transliterateString($men[$value]['name']);
            $ar['{! mfriend'.($key + 1).'.first_name !}'] = $this->transliterateString($men[$value]['first_name']);
            $ar['{! mfriend'.($key + 1).'.last_name !}'] = $this->transliterateString($men[$value]['last_name']);
            $ar['{! MFRIEND'.($key + 1).'.NAME !}'] = strtoupper($this->transliterateString($men[$value]['name']));
            $ar['{! MFRIEND'.($key + 1).'.FIRST_NAME !}'] = strtoupper($this->transliterateString($men[$value]['first_name']));
            $ar['{! MFRIEND'.($key + 1).'.LAST_NAME !}'] = strtoupper($this->transliterateString($men[$value]['last_name']));
        }

        if($opposite_gender == 'male'){
            foreach($mfriends as $key => $value){
                $ar['{! ofriend'.($key + 1).'.name !}'] = $this->transliterateString($men[$value]['name']);
                $ar['{! ofriend'.($key + 1).'.first_name !}'] = $this->transliterateString($men[$value]['first_name']);
                $ar['{! ofriend'.($key + 1).'.last_name !}'] = $this->transliterateString($men[$value]['last_name']);
                $ar['{! OFRIEND'.($key + 1).'.NAME !}'] = strtoupper($this->transliterateString($men[$value]['name']));
                $ar['{! OFRIEND'.($key + 1).'.FIRST_NAME !}'] = strtoupper($this->transliterateString($men[$value]['first_name']));
                $ar['{! OFRIEND'.($key + 1).'.LAST_NAME !}'] = strtoupper($this->transliterateString($men[$value]['last_name']));
            }
        }

        // Random Women
        foreach($ffriends as $key => $value){
            $ar['{! ffriend'.($key + 1).'.name !}'] = $this->transliterateString($women[$value]['name']);
            $ar['{! ffriend'.($key + 1).'.first_name !}'] = $this->transliterateString($women[$value]['first_name']);
            $ar['{! ffriend'.($key + 1).'.last_name !}'] = $this->transliterateString($women[$value]['last_name']);
            $ar['{! FFRIEND'.($key + 1).'.NAME !}'] = strtoupper($this->transliterateString($women[$value]['name']));
            $ar['{! FFRIEND'.($key + 1).'.FIRST_NAME !}'] = strtoupper($this->transliterateString($women[$value]['first_name']));
            $ar['{! FFRIEND'.($key + 1).'.LAST_NAME !}'] = strtoupper($this->transliterateString($women[$value]['last_name']));
        }

        if($opposite_gender == 'female'){
            foreach($ffriends as $key => $value){
                $ar['{! ofriend'.($key + 1).'.name !}'] = $this->transliterateString($women[$value]['name']);
                $ar['{! ofriend'.($key + 1).'.first_name !}'] = $this->transliterateString($women[$value]['first_name']);
                $ar['{! ofriend'.($key + 1).'.last_name !}'] = $this->transliterateString($women[$value]['last_name']);
                $ar['{! OFRIEND'.($key + 1).'.NAME !}'] = strtoupper($this->transliterateString($women[$value]['name']));
                $ar['{! OFRIEND'.($key + 1).'.FIRST_NAME !}'] = strtoupper($this->transliterateString($women[$value]['first_name']));
                $ar['{! OFRIEND'.($key + 1).'.LAST_NAME !}'] = strtoupper($this->transliterateString($women[$value]['last_name']));
            }
        }

        if(!empty($module_text)){
            foreach($module_text as $key => $value){
                foreach($value as $key2 => $value2){
                    if(is_numeric($key2)){
                        $ar['{! '.$key.'.'.$key2.' !}'] = $this->transliterateString($value2);
                        $ar[strtoupper('{! '.$key.'.'.$key2.' !}')] = strtoupper($this->transliterateString($value2));
                    }else{
                        // Traduction de value2
                        $translations = Translation::where('quizz_id', $answer->quizz->id)->where('lang', App::getLocale())->firstOrFail();
                        $translations = json_decode($translations->data);
                        $modules = json_decode($answer->builder)->modules;
                        foreach($modules as $key3 => $value3){
                            if($value3->name == $key){
                                $id_module = $value3->id;
                            }
                        }
                        if(empty($id_module)) $id_module = 0;
                        $fields = $modules[$id_module];
                        foreach($fields->schema as $key3 => $value3){
                            if($key2 == $value3->name) $id_field = $key3;
                        }
                        if(empty($id_field)) $id_field = 0;
                        $name_trans = 'answer'.$answer->id.'_module'.$id_module.'_'.$id_field.'_'.$value2['id'];
                        if(!empty($translations->$name_trans)){
                            $value2['name'] = $translations->$name_trans;
                        }
                        $ar['{! '.$key.'.'.$key2.' !}'] = $this->transliterateString($value2['name']);
                        $ar[strtoupper('{! '.$key.'.'.$key2.' !}')] = strtoupper($this->transliterateString($value2['name']));
                    }
                }
            }
        }

        $texte = str_replace(array_keys($ar), array_values($ar), $text);
        $texte = explode('|||', $texte);
        $texte = $texte[rand(0, count($texte) - 1)];
        $texte = explode(' ', $texte);
        foreach($texte as $key => $value){
            if(substr($value, 0, 2) == '#[' && substr($value, -1) == ']'){
                $inter = substr($value, 2, -1);
                $random = explode('||', $inter);
                $random = $random[rand(0, count($random) - 1)];
                $random = explode('-', $random);
                $texte[$key] = rand($random[0], $random[1]);
            }
        }
        $texte = implode(' ', $texte);
        return $texte;
    }

    public function buildPic($composant, $module_image, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic){
        $fake = null;
        $user = Auth::user();
        if($user->gender == 'female') $opposite_gender = 'male'; else $opposite_gender = 'female';
        if(!empty($composant->image)){
            $temp = Image::make('uploads/composant/'.$composant->image);
        }else{
            $ar = [];
            // Random Friends
            foreach($rfriends as $key => $value){
                $ar['{! friend'.($key + 1).'.avatar !}'] = $friends[$value]['picture'];
            }
            foreach($mfriends as $key => $value){
                $ar['{! mfriend'.($key + 1).'.avatar !}'] = $friends[$value]['picture'];
            }

            if($opposite_gender == 'male'){
                foreach($mfriends as $key => $value){
                    $ar['{! ofriend'.($key + 1).'.avatar !}'] = $friends[$value]['picture'];
                }
            }

            // Random Women
            foreach($ffriends as $key => $value){
                $ar['{! ffriend'.($key + 1).'.avatar !}'] = $friends[$value]['picture'];
            }

            if($opposite_gender == 'female'){
                foreach($ffriends as $key => $value){
                    $ar['{! ofriend'.($key + 1).'.avatar !}'] = $friends[$value]['picture'];
                }
            }
            if(array_key_exists($composant->object, $ar)){
                $temp = Image::make($ar[$composant->object]);
            }else if($composant->object == '{! me.avatar !}'){
                if($fake) $temp = $user->avatar = public_path('img/placeholder.jpg');
                $temp = Image::make(public_path('uploads/avatar/'.$user->avatar));
            }else{
                $verif_module = true;
                if(!empty($module_image)){
                    foreach($module_image as $key => $value){
                        foreach($value as $key2 => $value2){
                            if($composant->object == '{! '.$key.'.'.$key2.' !}'){
                                $temp = Image::make(public_path('uploads/module/'.$value2['name']));
                                $verif_module = false;
                            }
                        }
                    }
                }
                if($verif_module) $temp = Image::canvas($composant->width, $composant->height);
            }
        }
        $temp->resize($composant->width, $composant->height);

        // Mask
        if($composant->mask == 'circle'){
            $mask = Image::canvas($composant->width, $composant->height);
            $mask->circle($composant->width, $composant->width/2, $composant->height/2, function ($draw) {
                $draw->background('#fff');
            });
            $temp->mask($mask, false);
        }else if($composant->mask == 'custom'){
            $mask = Image::make(public_path('uploads/composant/'.$composant->custom_mask));
            $temp->mask($mask, false);
        }
        // Opacity
        if($composant->opacity != "100"){
            $temp->opacity($composant->opacity);
        }
        // Blur
        if($composant->blur != "0"){
            $temp->blur($composant->blur);
        }
        // Brightness
        if($composant->brightness != "0"){
            $temp->brightness($composant->brightness);
        }
        // Contrast
        if($composant->contrast != "0"){
            $temp->contrast($composant->contrast);
        }
        // Rotation
        if($composant->angle != "0"){
            $temp->rotate($composant->angle);
        }
        return $temp;
    }

    public function buildText($composant, $module_text, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic, $answer){
        $text = $this->completeText($composant->object, $module_text, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic, $answer);
        if(!empty($composant->back_color)){
            $temp = Image::canvas($composant->width, $composant->height, $composant->back_color);
        }else{
            $temp = Image::canvas($composant->width, $composant->height);
        }
        if($composant->horizontal_align == 'left'){
            $x = 0;
            $y = ($composant->height)/2;
        }else if($composant->horizontal_align == 'right'){
            $x = $composant->width;
            $y = ($composant->height)/2;
        }else{
            $x = ($composant->width)/2;
            $y = ($composant->height)/2;
        }
        if (App::getLocale() == 'ar' && !is_numeric($text)){
            $text = $Arabic->utf8Glyphs($text);
            $composant->font_family = public_path('fonts/ar.ttf');
        }
        $temp->text($text, $x, $y, function($font) use ($composant){
            $font->file($composant->font_family);
            $font->size($composant->font_size);
            $font->color($composant->font_color);
            $font->align($composant->horizontal_align);
            $font->valign($composant->vertical_align);
            $font->strokeWidth($composant->stroke_size);
            $font->strokeColor($composant->stroke_color);
        });
        if($composant->angle != "0"){
            $temp->rotate($composant->angle);
        }
        return $temp;
    }

    public function makeResult($answer, $friend, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $name=null){
        if(empty($name)) $name = null;
        $Arabic = new ArabicI18N('Glyphs');
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        $modules = $builder->modules;
        $module_text = array();
        $module_image = array();
        if(!empty($modules)){
            $var = 'id_key';
            foreach($modules as $key => $value){
                if($value->type == 'multivar'){
                    $value->data = array_values((array) $value->data);
                    // On retire d'abord les résultats qui ne correspondent pas à notre sexe avant de faire de l'aléatoire
                    foreach($value->data as $key2 => $value2){
                        if($value2->params_gender != 'all' && $value2->params_gender != Auth::user()->gender){
                            unset($value->data[$key2]);
                        }else{
                            $value->data[$key2]->$var = $key2;
                        }
                    }
                    $id_module = rand(0, count($value->data) - 1);
                    $data = $value->data[$id_module];
                    foreach($value->schema as $key2 => $value2){
                        if($value2->type == 'text'){
                            $key_name = $value2->name;
                            $module_text[$value->name][$key_name] = ['name' => $data->$key_name, 'id' => $data->$var];
                        }else if($value2->type == 'picture'){
                            $key_name = $value2->name;
                            $module_image[$value->name][$key_name] = ['name' => $data->$key_name, 'id' => $data->$var];
                        }
                    }
                }elseif($value->type == 'percent'){
                    $number_of_groups   = $value->parts;
                    $sum_to             = 100;
                    $groups             = array();
                    $group              = 0;

                    while(array_sum($groups) != $sum_to)
                    {
                        $module_text[$value->name][$group + 1] = mt_rand(0, $sum_to/mt_rand(1,5));
                        $groups[$group] = mt_rand(0, $sum_to/mt_rand(1,5));
                        if(++$group == $number_of_groups)
                        {
                            $group  = 0;
                        }
                    }
                }
            }
        }

        // On trie les composants par index
        usort($composants, function($a, $b) {
            return $a->index <=> $b->index;
        });
        $img = Image::canvas(1200, 630);
        foreach($composants as $key => $value) {
            $value->width = round(($value->width / 100) * 1200, 0);
            $value->pos_x = round(($value->pos_x / 100) * 1200, 0);
            $value->height = round(($value->height / 100) * 630, 0);
            $value->pos_y = round(($value->pos_y / 100) * 630, 0);
        }
        // On regarde les images qui sont en dessous
        foreach($composants as $key => $value){
            // Si l'image se trouve en dessous
            if($value->b_o_f == "back"){
                if($value->type == 'picture'){
                    $img->insert($this->buildPic($value, $module_image, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic), 'top-left', $value->pos_x, $value->pos_y);
                }else{
                    // On récupère la traduction
                    $translations = Translation::where('quizz_id', $answer->quizz->id)->where('lang', App::getLocale())->firstOrFail();
                    $translations = json_decode($translations->data);
                    $name_trans = 'answer'.$answer->id.'_composant'.$value->id;
                    if(!empty($translations->$name_trans)){
                            $value->object = $translations->$name_trans;
                    }
                    $img->insert($this->buildText($value, $module_text, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic, $answer), 'top-left', $value->pos_x, $value->pos_y);
                }
            }
        }
        // On met l'image de fond
        if(!empty($answer->background)) $img->insert(Image::make(public_path('uploads/answer/'.$answer->background)));
        // On regarde les images qui sont au dessus
        foreach($composants as $key => $value){
            // Si l'image se trouve en dessous
            if($value->b_o_f == "front"){
                if($value->type == 'picture'){
                    $img->insert($this->buildPic($value, $module_image, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic), 'top-left', $value->pos_x, $value->pos_y);
                }else{
                    // On récupère la traduction
                    $translations = Translation::where('quizz_id', $answer->quizz->id)->where('lang', App::getLocale())->firstOrFail();
                    $translations = json_decode($translations->data);
                    $name_trans = 'answer'.$answer->id.'_composant'.$value->id;
                    if(!empty($translations->$name_trans)){
                            $value->object = $translations->$name_trans;
                    }
                    $img->insert($this->buildText($value, $module_text, $friends, $rfriends, $mfriends, $ffriends, $men, $women, $Arabic, $answer), 'top-left', $value->pos_x, $value->pos_y);
                }
            }
        }
        // On insert le watermark
        //$img->insert(Image::make(public_path('img/watermark.png')), 'top-left', 3, 3);
        // On supprime l'ancienne image si elle existe
        if(empty($name)){
            $slug = str_random(rand(2,6)).time().str_random(rand(3,9));
            $result_name = 'r_'.time().str_random(20).'.jpg';
        }else{
            $slug = 'answer_g_'.$name;
            $result_name = $slug.'.jpg';
        }
        $img->save(public_path('uploads/result/'.$result_name));
        $result = new Result();
        $result->quizz_id = $answer->quizz_id;
        if(Auth::check()) $result->user_id = Auth::user()->id;
        $result->slug = $slug;
        $result->image = $result_name;
        $result->lang = App::getLocale();
        $result->ip = $this->get_client_ip();
        $result->save();
        return $result;
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function newToken(LaravelFacebookSdk $fb){
        if(Auth::check()){
            $user = Auth::user();
        }else{
            return redirect('/auth/facebook');
        }
        $token = $user->fb_token;
        if(!$token) return redirect('/auth/facebook');
        $oauth_client = $fb->getOAuth2Client();
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        }catch(Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return redirect('/auth/facebook')->with('alert_error');
        }

        $fb->setDefaultAccessToken($token);
        $user->fb_token = (string) $token;
        $user->save();

        return $token;
    }

    public function checkPermissions($fb){
        $token = $this->newToken($fb);
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);
        try {
            $response = $fb->get('/me/permissions');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return back()->with('error_fb', true);
        }
        $permissions = $response->getGraphEdge();
        $autorization = true;
        foreach($permissions as $permission){
            if($permission['status'] == 'declined') $autorization = false;
        }
        return $autorization;
    }

    function transliterateString($txt){
        $transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
        return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
    }

    function getGender($name){
        $users = User::where('first_name', $name);
        $male = 0;
        $female = 0;
        foreach($users as $user){
            if($user->gender == 'female'){
                $female++;
            }else{
                $male++;
            }
        }
        if($male >= $female) return 'male'; else return 'female';
    }

}
