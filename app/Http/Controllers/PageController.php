<?php

namespace App\Http\Controllers;

use App\Stats;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tag;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Quizz;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{
    public function index(Request $request){
        if(!empty($request->session()->get('fb_url'))){
            $quizz_url = $request->session()->pull('fb_url');
            $slug = explode('/q/', $quizz_url);
            if(!empty($slug[1])) return redirect('/dq/'.$slug[1]);
        }
        $tags = Tag::join('translations', 'translations.tag_id', '=', 'tags.id')
            ->where('tags.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('tags.visible', true)
            ->orderBy('tags.created_at', 'desc')
            ->select('translations.*', 'tags.*')
            ->with('translations')
            ->simplePaginate(18);
        if ($request->ajax()) {
            return Response::json(View::make('front.pages.quizzs', array('tags' => $tags))->render());
        }
        return view('front.pages.home', compact('tags'));
    }

    public function me(){
        return view('front.pages.me');
    }

    public function search(Request $request){
        $tags = Quizz::join('translations', 'translations.tag_id', '=', 'tags.id')
            ->where('quizzs.langs', 'LIKE', '%'.App::getLocale().'%')
            ->where('translations.lang', App::getLocale())
            ->where('translations.visible', true)
            ->where('quizzs.visible', true)
            ->where('translations.data', 'LIKE', '%'. Input::get('tag').'%')
            ->with('translations')
            ->orderBy('quizzs.created_at', 'desc')
            ->select('translations.*', 'quizzs.*')
            ->simplePaginate(18);
        $search_tag = Input::get('tag');
        return view('front.pages.home', compact('tags', 'search_tag'));
    }

    public function terms(){
        return view('front.pages.terms');
    }

    public function privacy(){
        return view('front.pages.privacy');
    }

    public function legal(){
        return view('front.pages.legal');
    }

    public function delete(){
        return view('front.pages.delete');
    }

    public function confirmDelete(){
        $user = Auth::user();
        if(file_exists(public_path('uploads/avatar/'.$user->avatar))) unlink(public_path('uploads/avatar/'.$user->avatar));
        $results = $user->results()->get();
        foreach($results as $result){
            if(file_exists(public_path('uploads/result/'.$result->image))) unlink(public_path('uploads/result/'.$result->image));
        }
        $user->results()->delete();
        $user->delete();
        Auth::logout();
        return redirect('/')->with('account_deleted', true);
    }

    public function updateShareQuizz($quizz_id){
        $stats = new Stats();
        $stats->quizz = $quizz_id;
        $stats->user = Auth::user()->id;
        $stats->type = 'share';
        $stats->ip = request()->ip();
        $stats->save();
        return ['success' => true];
    }
}
