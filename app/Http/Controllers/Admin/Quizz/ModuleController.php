<?php

namespace App\Http\Controllers\Admin\Quizz;

use App\Answer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ModuleController extends Controller
{
    public function create($quizz_id, $answer_id){
        $answer = Answer::find($answer_id);
        $quizz = $answer->quizz;
        return view('back.quizz.answer.module.create', compact('answer', 'quizz'));
    }

    /**
     * @param Request $request
     * @param $quizz_id
     * @param $answer_id
     */
    public function store(Request $request, $quizz_id, $answer_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $modules = $builder->modules;
        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
        $rules = [
            'name' => 'required|without_spaces',
            'type' => 'required|in:multivar,percent'
        ];
        if(Input::get('type') == 'multivar'){
            foreach($request->all() as $key => $value){
                if(strpos($key, 'elements_') !== false){
                    $rules[$key] = 'without_spaces';
                }else if(strpos($key, 'types_') !== false){
                    $rules[$key] = 'without_spaces|in:picture,text';
                }
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }else {
                $module = collect();
                $module->put('id', count($modules));
                $module->put('name', Input::get('name'));
                $module->put('type', Input::get('type'));
                $schema = array();
                foreach($request->all() as $key => $value){
                    if(strpos($key, 'elements_') !== false){
                        $number = explode('elements_', $key)[1];
                        $schema[] = ['name' => $value, 'type' => (!empty(Input::get('types_'.$number))) ? Input::get('types_'.$number) : 'picture'];
                    }
                }
                $module->put('schema', $schema);
            }
        }else if(Input::get('type') == 'percent'){
            $rules['parts'] = 'required|min:1|max:100|numeric';
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }else {
                $module = collect();
                $module->put('id', count($modules));
                $module->put('name', Input::get('name'));
                $module->put('type', Input::get('type'));
                $module->put('parts', Input::get('parts'));
            }
        }else{
            $validator = Validator::make($request->all(), $rules);
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $modules = (array) $modules;
        array_push($modules, $module);
        $builder->modules = $modules;
        $answer->builder = json_encode($builder);
        $answer->save();
        return redirect('quizz/'.$quizz_id.'/answer/'.$answer_id)->with('added_module', true);
    }

    public function edit($quizz_id, $answer_id, $module_id){
        $answer = Answer::findOrFail($answer_id);
        $modules = json_decode($answer->builder)->modules;
        $module = $modules[$module_id];
        return view('back.quizz.answer.module.edit', compact('answer', 'module'));
    }

    public function update(Request $request, $quizz_id, $answer_id, $module_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $modules = $builder->modules;
        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
        $rules = [
            'name' => 'required|without_spaces',
            'type' => 'required|in:multivar,percent'
        ];
        if(Input::get('type') == 'multivar'){
            foreach($request->all() as $key => $value){
                if(strpos($key, 'elements_') !== false){
                    $rules[$key] = 'without_spaces';
                }else if(strpos($key, 'types_') !== false){
                    $rules[$key] = 'without_spaces|in:picture,text';
                }
            }
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }else {
                $module = collect();
                $module->put('id', count($modules));
                $module->put('name', Input::get('name'));
                $module->put('type', Input::get('type'));
                $schema = array();
                foreach($request->all() as $key => $value){
                    if(strpos($key, 'elements_') !== false){
                        $number = explode('elements_', $key)[1];
                        // BUG AU NIVEAU DES BOUTONS RADIO ON RECUPERE DIRECT EN BDD
                        if(!empty(Input::get('old_'.$number))){
                            $schema[] = ['name' => $value, 'type' => $modules[$module_id]->schema[$number - 1]->type];
                        }else{
                            $schema[] = ['name' => $value, 'type' => (!empty(Input::get('types_'.$number))) ? Input::get('types_'.$number) : 'picture'];
                        }
                    }
                }
                $old_schem = $modules[$module_id]->schema;
                $modules[$module_id]->schema = $schema;

                foreach($schema as $key => $value){
                    if(!empty($old_schem[$key]) && $value['name'] != $old_schem[$key]->name && !empty(Input::get('old_'.($key + 1)))) {
                        if(!empty($modules[$module_id]->data)){
                            foreach($modules[$module_id]->data as $key2 => $value2){
                                $old_name = $old_schem[$key]->name;
                                $new_name = $value['name'];
                                $modules[$module_id]->data[$key2]->$new_name = $value2->$old_name;
                                unset($modules[$module_id]->data[$key2]->$old_name);
                            }
                        }
                    }else if(!empty($old_schem[$key]) && empty(Input::get('old_'.($key + 1)))){
                        if(!empty($modules[$module_id]->data)) {
                            $old_name = $old_schem[$key]->name;
                            $new_name = $value['name'];
                            foreach ($modules[$module_id]->data as $key2 => $value2) {
                                unset($modules[$module_id]->data[$key2]->$old_name);
                                $modules[$module_id]->data[$key2]->$new_name = null;
                            }
                        }
                    }else if(empty($old_schem[$key])){
                        if(!empty($modules[$module_id]->data)) {
                            foreach ($modules[$module_id]->data as $key2 => $value2) {
                                $new_name = $value['name'];
                                $modules[$module_id]->data[$key2]->$new_name = null;
                            }
                        }
                    }
                }

                // Si on a supprimé un élèment
                if(count($old_schem) > count($schema)){
                    foreach($old_schem as $key => $value){
                        if(!empty($modules[$module_id]->data)) {
                            foreach ($modules[$module_id]->data as $key2 => $value2) {
                                if(empty($schema[$key])){
                                    $old_name = $old_schem[$key]->name;
                                    unset($modules[$module_id]->data[$key2]->$old_name);
                                }
                            }
                        }
                    }
                }

            }
        }else if(Input::get('type') == 'percent'){
            $rules['parts'] = 'required|min:1|max:100|numeric';
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }else {
                $modules[$module_id]->name = Input::get('name');
                $modules[$module_id]->parts = Input::get('parts');
            }
        }
        $modules = (array) $modules;
        $builder->modules = $modules;
        $answer->builder = json_encode($builder);
        $answer->save();
        return redirect('quizz/'.$quizz_id.'/answer/'.$answer_id)->with('updated_module', true);
    }

    public function getSetting($quizz_id, $answer_id, $module_id){
        $answer = Answer::findOrFail($answer_id);
        $quizz = $answer->quizz;
        $module = json_decode($answer->builder)->modules[$module_id];
        return view('back.quizz.answer.module.setting', compact('answer', 'quizz', 'module_id', 'module'));
    }

    public function postSetting(Request $request, $quizz_id, $answer_id, $module_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $schema = $builder->modules[$module_id]->schema;
        if(!empty($builder->modules[$module_id]->data)){
            $datas = $builder->modules[$module_id]->data;
        }else{
            $datas = [];
        }

        $rules = ['params_gender' => 'required|in:all,male,female'];
        foreach($schema as $key => $value){
            if($value->type == 'picture'){
                $rules['image_'.$value->name] = 'required|image';
            }else if($value->type == 'text'){
                $rules['text_'.$value->name] = 'required';
            }
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else {
            $data = array();
            foreach($schema as $key => $value){
                if($value->type == 'picture'){
                    if($request->hasFile('image_').$value->name){
                        $image_name =  'm_'.time().str_random(rand(15, 20)).'.png';
                        $image = Image::make(Input::file('image_'.$value->name))->save(public_path('uploads/module/'.$image_name));
                        $data[$value->name] = $image_name;
                    }
                }else if($value->type == 'text'){
                    $data[$value->name] = Input::get('text_'.$value->name);
                }
            }
            $data['params_gender'] = Input::get('params_gender');
            ksort($data);
            array_push($datas, $data);
            $builder->modules[$module_id]->data = $datas;
            $answer->builder = json_encode($builder);
            $answer->save();

            return redirect('quizz/'.$quizz_id.'/answer/'.$answer_id.'/module/'.$module_id.'/setting')->with('added_data', true);
        }
    }

    public function getDataEdit($quizz_id, $answer_id, $module_id, $data_key){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $schema = $builder->modules[$module_id]->schema;
        $data = $builder->modules[$module_id]->data[$data_key];
        return view('back.quizz.answer.module.setting.edit', compact('answer', 'data', 'schema', 'module_id', 'data_key'));
    }

    public function postDataEdit(Request $request, $quizz_id, $answer_id, $module_id, $data_key)
    {
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $schema = $builder->modules[$module_id]->schema;
        if (!empty($builder->modules[$module_id]->data)) {
            $datas = $builder->modules[$module_id]->data;
        } else {
            $datas = [];
        }

        $rules = ['params_gender' => 'required|in:all,male,female'];
        foreach ($schema as $key => $value) {
            if ($value->type == 'picture') {
                $rules['image_' . $value->name] = 'image';
            } else if ($value->type == 'text') {
                $rules['text_' . $value->name] = 'required';
            }
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $data = array();
            foreach ($schema as $key => $value) {
                $value_name = $value->name;
                if ($value->type == 'picture') {
                    if ($request->hasFile('image_' . $value->name)) {
                        if(file_exists(public_path('uploads/module/'.$datas[$data_key]->$value_name))){
                            unlink(public_path('uploads/module/'.$datas[$data_key]->$value_name));
                        }
                        $image_name = 'm_' . time() . str_random(rand(15, 20)) . '.png';
                        $image = Image::make(Input::file('image_' . $value->name))->save(public_path('uploads/module/' . $image_name));
                        $data[$value->name] = $image_name;
                    }else{
                        $data[$value->name] = $datas[$data_key]->$value_name;
                    }
                } else if ($value->type == 'text') {
                    $data[$value->name] = Input::get('text_' . $value->name);
                }
            }
            $data['params_gender'] = Input::get('params_gender');
            ksort($data);
            $datas[$data_key] = $data;
            $builder->modules[$module_id]->data = $datas;
            $answer->builder = json_encode($builder);
            $answer->save();

            return redirect('quizz/' . $quizz_id . '/answer/' . $answer_id . '/module/' . $module_id . '/setting')->with('updated_data', true);

        }
    }

    public function deleteData($quizz_id, $answer_id, $module_id, $data_key){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $datas = $builder->modules[$module_id]->data;
        $datas = (array) $datas;
        $datas = array_values($datas);
        foreach($datas[$data_key] as $key => $value){
            if(substr($value, -4) == '.png' && file_exists(public_path('uploads/module/'.$value))){
                unlink(public_path('uploads/module/'.$value));
            }
        }
        unset($datas[$data_key]);
        $builder->modules[$module_id]->data = $datas;
        $answer->builder = json_encode($builder);
        $answer->save();
        return redirect('quizz/' . $quizz_id . '/answer/' . $answer_id . '/module/' . $module_id . '/setting')->with('deleted_data', true);
    }

    public function destroy(Request $request, $quizz_id, $answer_id, $module_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $modules = $builder->modules;
        $new_modules = collect();
        $module = $modules[$module_id];
        // Suppresion des images associées
        if(!empty($module->data)){
            foreach($module->data as $key => $value){
                foreach($value as $key2 => $value2){
                    if(substr($value2, -4) == '.png' && file_exists(public_path('uploads/module/'.$value2))){
                        unlink(public_path('uploads/module/'.$value2));
                    }
                }
            }
        }
        foreach($modules as $key => $value){
            if($key > $module_id){
                $modules[$key]->id--;
                $new_modules->push($modules[$key]);
            }else if($key < $module_id){
                $new_modules->push($modules[$key]);
            }
        }
        $builder->modules = $new_modules;
        $answer->builder = json_encode($builder);
        $answer->save();
        return ['success' => true];
    }
}
