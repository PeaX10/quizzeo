<?php

namespace App\Http\Controllers\Admin\Quizz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Answer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use App\Translation;
class ComposantController extends Controller
{
    public function create($quizz_id, $answer_id){
        $answer = Answer::findOrFail($answer_id);
        return view('back.quizz.answer.composant.create', compact('answer'));
    }

    public function store(Request $request, $quizz_id, $answer_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required|in:picture,text',
            'width' => 'required|numeric|between:1,1200',
            'height' => 'required|numeric|between:1,630',
            'top' => 'required|numeric|between:-630,630',
            'left' => 'required|numeric|between:-1200,1200',
            'angle' => 'required|numeric|between:-360,360',
            'index' => 'required|numeric|min:1',
            'b_o_f' => 'required|in:back,front',
            'font-size' => 'required_if:type,text|numeric|between:1,500',
            'font-family' => 'required_if:type,text',
            'text-color' => 'required_if:type,text|size:7',
            'back-color' => 'size:7',
            'vertical_align' => 'required_if:type,text|in:top,middle,bottom',
            'horizontal_align' => 'required_if:type,text|in:left,center,right',
            'stroke-size' => 'required_if:type,text|numeric|between:0,360',
            'stroke-color' => 'required_if:type,text|size:7',
            'mask' => 'required_if:type,picture|in:regular,circle,custom',
            'custom-mask' => 'required_if:mask,custom|image|max:8192',
            'opacity' => 'required_if:type,picture|numeric|between:0,100',
            'blur' => 'required_if:type,picture|numeric|between:0,100',
            'brightness' => 'required_if:type,picture|numeric|between:-100,100',
            'contrast' => 'required_if:type,picture|numeric|between:-100,100',
            'object' => '',
            'image' => 'image|max:8192'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $composant = collect();
            $composant->put('id', count($composants));
            $composant->put('name', Input::get('name'));
            $composant->put('type', Input::get('type'));
            $composant->put('pos_x', (Input::get('left') / 1200) * 100);
            $composant->put('pos_y', (Input::get('top') / 630) * 100);
            $composant->put('width', (Input::get('width') / 1200) * 100);
            $composant->put('height', (Input::get('height') / 630) * 100);
            $composant->put('angle', Input::get('angle'));
            $composant->put('index', Input::get('index'));
            $composant->put('b_o_f', Input::get('b_o_f'));
            if($composant['type'] == 'picture'){
                $composant->put('mask', Input::get('mask'));
                if($composant['mask'] == 'custom'){
                    $mask_name = 'mask_'.time().str_random(30).'.png';
                    Image::make(Input::file('custom-mask'))->save(public_path('uploads/composant/'.$mask_name));
                    $composant->put('custom_mask', $mask_name);
                }
                $composant->put('opacity', Input::get('opacity'));
                $composant->put('blur', Input::get('blur'));
                $composant->put('contrast', Input::get('contrast'));
                $composant->put('brightness', Input::get('brightness'));
            }else{
                $composant->put('font_size', Input::get('font-size'));
                $composant->put('font_color', Input::get('text-color'));
                $composant->put('back_color', Input::get('back-color'));
                $composant->put('font_family', Input::get('font-family'));
                $composant->put('vertical_align', Input::get('vertical_align'));
                $composant->put('horizontal_align', Input::get('horizontal_align'));
                $composant->put('stroke_size', Input::get('stroke-size'));
                $composant->put('stroke_color', Input::get('stroke-color'));
            }
            if($composant['type'] == 'picture' && $request->hasFile('image')){
                $image_name = 'image_'.time().str_random(30).'.png';
                Image::make(Input::file('image'))->save(public_path('uploads/composant/'.$image_name));
                $composant->put('image', $image_name);
            }else{
                $composant->put('object', Input::get('object'));
                if($this->is_url_exist($composant['object'])) {
                    $image_name = 'image_'.time().str_random(30).'.png';
                    Image::make($composant['object'])->save(public_path('uploads/composant/'.$image_name));
                    $composant->put('image', $image_name);
                }
            }
            $composants = (array) $composants;
            array_push($composants, $composant);
            $builder->composants = $composants;
            $answer->builder = json_encode($builder);
            $answer->save();
            return redirect('quizz/'.$quizz_id.'/answer/'.$answer_id)->with('added_composant', true);
        }
    }

    function is_url_exist($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
            $status = true;
        }else{
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    public function destroy(Request $request, $quizz_id, $answer_id, $composant_id){
        if($request->ajax()){
            $answer = Answer::findOrFail($answer_id);
            $translations = Translation::where('quizz_id', $quizz_id)->get();
            $builder = json_decode($answer->builder);
            $composants = $builder->composants;
            $new_composants = collect();
            foreach($composants as $key => $value){
                if(!empty($value->image) && file_exists(public_path('uploads/composant/'.$value->image)) && $value->id == $composant_id){
                    unlink(public_path('uploads/composant/'.$value->image));
                }
                if(!empty($value->custom_mask) && file_exists(public_path('uploads/composant/'.$value->custom_mask)) && $value->id == $composant_id){
                    unlink(public_path('uploads/composant/'.$value->custom_mask));
                }
                if($key > $composant_id){
                    $composants[$key]->id--;
                    $new_composants->push($composants[$key]);
                }else if($key < $composant_id){
                    $new_composants->push($composants[$key]);
                }
            }
            foreach($translations as $key => $value){
                $data = json_decode($value->data);
                foreach($data as $key2 => $value2){
                    $id_extracted = explode('answer'.$answer_id.'_composant', $key2);
                    if(!empty($id_extracted[1]) && is_numeric($id_extracted[1])){
                        $id_extracted = $id_extracted[1];
                    }else{
                        $id_extracted = null;
                    }
                    if($id_extracted && $id_extracted == $composant_id){
                        unset($data[$key2]);
                    }else if($id_extracted > $composant_id){
                        $data['answer'.$answer_id.'_composant'.($id_extracted - 1)] = $data['answer'.$answer_id.'_composant'.$id_extracted];
                        unset($data['answer'.$answer_id.'_composant'.$id_extracted]);
                    }
                }
                $value->data = json_encode($data);
                $value->save();
            }
            $builder->composants = $new_composants;
            $answer->builder = json_encode($builder);
            $answer->save();
            return ['success' => true];
        }
    }

    public function edit($quizz_id, $answer_id, $composant_id){
        $answer = Answer::findOrFail($answer_id);
        $composants = json_decode($answer->builder)->composants;
        $composant = $composants[$composant_id];
        $composant->width = round(($composant->width / 100)*1200, 0);
        $composant->pos_x = round(($composant->pos_x / 100)*1200);
        $composant->height = round(($composant->height / 100)*630);
        $composant->pos_y = round(($composant->pos_y / 100)*630);
        $var = 'font-family';
        if(!empty($composant->font_family)) $composant->$var = $composant->font_family;
        return view('back.quizz.answer.composant.edit', compact('answer', 'composant'));
    }

    public function update(Request $request, $quizz_id, $answer_id, $composant_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        $old_composant = $composants[$composant_id];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required|in:picture,text',
            'width' => 'required|numeric|between:1,1200',
            'height' => 'required|numeric|between:1,1200',
            'top' => 'required|numeric|between:-630,630',
            'left' => 'required|numeric|between:-1200,1200',
            'angle' => 'required|numeric|between:-360,360',
            'index' => 'required|numeric|min:1',
            'b_o_f' => 'required|in:back,front',
            'font-size' => 'required_if:type,text|numeric|between:1,500',
            'font-family' => 'required_if:type,text',
            'text-color' => 'required_if:type,text|size:7',
            'vertical_align' => 'required_if:type,text|in:top,middle,bottom',
            'horizontal_align' => 'required_if:type,text|in:left,center,right',
            'stroke-size' => 'required_if:type,text|numeric|between:0,360',
            'stroke-color' => 'required_if:type,text|size:7',
            'mask' => 'required_if:type,picture|in:regular,circle,custom',
            'custom-mask' => 'image|max:8192',
            'opacity' => 'required_if:type,picture|numeric|between:0,100',
            'blur' => 'required_if:type,picture|numeric|between:0,100',
            'brightness' => 'required_if:type,picture|numeric|between:-100,100',
            'contrast' => 'required_if:type,picture|numeric|between:-100,100',
            'object' => '',
            'image' => 'image|max:8192'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $composant = collect();
            $composant->put('id', $old_composant->id);
            $composant->put('name', Input::get('name'));
            $composant->put('type', Input::get('type'));
            $composant->put('pos_x', (Input::get('left') / 1200) * 100);
            $composant->put('pos_y', (Input::get('top') / 630) * 100);
            $composant->put('width', (Input::get('width') / 1200) * 100);
            $composant->put('height', (Input::get('height') / 630) * 100);
            $composant->put('angle', Input::get('angle'));
            $composant->put('index', Input::get('index'));
            $composant->put('b_o_f', Input::get('b_o_f'));
            if($composant['type'] == 'picture'){
                $composant->put('mask', Input::get('mask'));
                if($composant['mask'] == 'custom' && $request->hasFile('custom-mask')){
                    if(!empty($old_composant->custom_mask)) unlink(public_path('uploads/composant/'.$old_composant->custom_mask));
                    $mask_name = 'mask_'.time().str_random(30).'.png';
                    Image::make(Input::file('custom-mask'))->save(public_path('uploads/composant/'.$mask_name));
                    $composant->put('custom_mask', $mask_name);
                }
                $composant->put('opacity', Input::get('opacity'));
                $composant->put('blur', Input::get('blur'));
                $composant->put('contrast', Input::get('contrast'));
                $composant->put('brightness', Input::get('brightness'));
            }else{
                $composant->put('font_size', Input::get('font-size'));
                $composant->put('font_color', Input::get('text-color'));
                $composant->put('back_color', Input::get('back-color'));
                $composant->put('font_family', Input::get('font-family'));
                $composant->put('vertical_align', Input::get('vertical_align'));
                $composant->put('horizontal_align', Input::get('horizontal_align'));
                $composant->put('stroke_size', Input::get('stroke-size'));
                $composant->put('stroke_color', Input::get('stroke-color'));
            }
            if($composant['type'] == 'picture' && $request->hasFile('image')){
                if(!empty($old_composant->image)) unlink(public_path('uploads/composant/'.$old_composant->image));
                $image_name = 'image_'.time().str_random(30).'.png';
                Image::make(Input::file('image'))->save(public_path('uploads/composant/'.$image_name));
                $composant->put('image', $image_name);
            }else{
                $composant->put('object', Input::get('object'));
                if($this->is_url_exist($composant['object'])) {
                    if(!empty($old_composant->image)) unlink(public_path('uploads/composant/'.$old_composant->image));
                    $image_name = 'image_'.time().str_random(30).'.png';
                    Image::make($composant['object'])->save(public_path('uploads/composant/'.$image_name));
                    $composant->put('image', $image_name);
                }else if(empty($composant->object) && !empty($old_composant->image)){
                    $composant->put('image', $old_composant->image);
                }
            }
            $composants = (array) $composants;
            foreach($composants as $key => $value){
                if($key == $composant_id){
                    $composants[$key] = $composant;
                }
            }
            $builder->composants = $composants;
            $answer->builder = json_encode($builder);
            $answer->save();
            return redirect('quizz/'.$quizz_id.'/answer/'.$answer_id)->with('updated_composant', true);
        }
    }
}
