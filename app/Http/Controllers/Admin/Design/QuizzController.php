<?php

namespace App\Http\Controllers\Admin\Design;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Quizz;
use App\DesignerValidation;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class QuizzController extends Controller
{
    public function index(){
        $quizzs = Quizz::all();
        return view('design.quizz.index', compact('quizzs'));
    }

    public function edit($id){
        $quizz = quizz::findOrFail($id);
        $answers = $quizz->answers()->orderBy('created_at', 'desc')->get();
        $validaton = DesignerValidation::where('user_id', Auth::user()->id)->where('type', 'quizz')->where('design_id', $id)->where('validated', 0)->first();
        if(!empty($validaton)){
            $data = json_decode($validaton->data);
            $image = $data->image;
            $playImage = $data->playImage;
        }
        return view('design.quizz.edit', compact('quizz', 'answers', 'image', 'playImage'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|max:8192|dimensions:min_width=400,min_height=400',
            'playImage' => 'required|image|max:8192|dimensions:min_width=1200,min_height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $valid = DesignerValidation::where('user_id', Auth::user()->id)->where('type', 'quizz')->where('design_id', $id)->where('validated', 0);
            if(!$valid->exists()){
                $validation = new DesignerValidation();
            }else{
                $validation = $valid->first();
            }
            $validation->user_id = Auth::user()->id;
            $data = [];
            if($request->hasFile('image')){
                $ext = Input::file('image')->getClientOriginalExtension();
                $image_name =  'q_'.time().str_random(rand(15, 20)).'.'.$ext;
                if($ext != 'gif'){
                    $image = Image::make(Input::file('image'))->resize(400,400)->save(public_path('uploads/quizz/'.$image_name));
                }else{
                    Input::file('image')->resize(400, 400)->move(public_path('uploads/quizz'), $image_name);
                }
                $data['image'] = $image_name;
            }
            if($request->hasFile('playImage')){
                $ext = Input::file('playImage')->getClientOriginalExtension();
                $image_name =  'q_'.time().str_random(rand(15, 20)).'.'.$ext;
                if($ext != 'gif'){
                    $image = Image::make(Input::file('playImage'))->save(public_path('uploads/quizz/'.$image_name));
                }else{
                    Input::file('playImage')->move(public_path('uploads/quizz'), $image_name);
                }
                $data['playImage'] = $image_name;
            }
            $validation->data = json_encode($data);
            $validation->type = 'quizz';
            $validation->design_id = $id;
            $validation->save();
            return back()->with('edited_quizz', true);

        }
    }
    
    public function design($quizz_id, $answer_id){
        $answer = Answer::findOrFail($answer_id);
        $builder = json_decode($answer->builder);
        $composants = $builder->composants;
        return view('back.quizz.answer.show', compact('answer', 'composants', 'builder'));
    }
    
    
}
