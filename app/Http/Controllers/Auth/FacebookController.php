<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\GraphNodes\GraphUser;
use App\User;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class FacebookController extends Controller
{

    public function login(LaravelFacebookSdk $fb){
        session(['fb_url' => URL::previous()]);
        return redirect($fb->getLoginUrl(['email', 'user_friends']));
    }

    public function callback(LaravelFacebookSdk $fb){
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        if (!$token) {
            $helper = $fb->getRedirectLoginHelper();

            if (!$helper->getError()) {
                abort(403, 'Unauthorized action.');
            }
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        if (!$token->isLongLived()) {
            $oauth_client = $fb->getOAuth2Client();

            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            }catch(Facebook\Exceptions\FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);

        try {
            $response = $fb->get('/me?fields=id,name,email,picture.type(square).width(1200).height(1200),first_name,last_name,gender');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
        $fb_user = $response->getGraphUser();
        
        $user = $this->createOrUpdateFacebookUser($fb_user);
        Auth::login($user, true);
        $user->fb_token = (string) $token;
        $this->syncFriends($fb, false);
        return redirect('/')->with('fb_login', true);
    }

    public function createOrUpdateFacebookUser(GraphUser $fb_user){
        $user = User::where('fb_id', $fb_user->getID());
        if($user->exists()){
            $user = $user->first();
            if(file_exists(public_path('uploads/avatar/'.$user->avatar))) unlink(public_path('uploads/avatar/'.$user->avatar));
        }else {
            $user = new User();
            $user->fb_id = $fb_user->getID();
        }
        $user->name = $fb_user->getName();
        $user->first_name = $fb_user->getFirstName();
        $user->last_name = $fb_user->getLastName();
        $user->email = ($fb_user->getEmail()) ? $fb_user->getEmail() : 'NO_EMAIL';
        if(empty($fb_user->getGender())) $user->gender = $this->guessGender($user->first_name); else $user->gender = $fb_user->getGender();
        $avatar_name = str_slug($user->name).'_'.str_random(40).'.jpg';
        Image::make($fb_user->getPicture()->getUrl())->save(public_path('uploads/avatar/'.$avatar_name));
        $user->avatar = $avatar_name;
        $user->save();
        return $user;
    }

    public function syncFriends(LaravelFacebookSdk $fb, $redirect = true){
        $user = Auth::user();
        if(!$this->checkPermissions($fb)) return redirect($fb->getReRequestUrl(['email', 'user_friends']));
        $friends = $this->getFriendsList($fb);
        $friends = json_encode($friends);
        $user->friends = $friends;
        $user->save();
        if($redirect) return back()->with('sync_friends', true);
    }

    public function getFriendsList(LaravelFacebookSdk $fb){
        $token = Auth::user()->fb_token;
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);

        try {
            $response = $fb->get('/me/taggable_friends?fields=id,name,first_name,last_name,gender,picture.type(square).width(20).height(20)&limit=1000');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            return back()->with('error_fb', true);
        }
        $friends = array();
        $end = true;
        $fb_friends = $response->getGraphEdge();
        while($end){
            foreach($fb_friends as $friend) {
                $data = [
                    'id' => $friend->getField('id'),
                    'name' => $friend->getField('name'),
                    'first_name' => $friend->getField('first_name'),
                    'last_name' => $friend->getField('last_name'),
                    'gender' => $friend->getField('gender'),
                    'picture' => $friend->getField('picture')->getField('url')
                ];
                array_push($friends, $data);
            }
            if(isset($fb_friends->getMetaData('paging')['paging']['next'])){
                $after = $fb_friends->getMetaData('paging')['paging']['cursors']['after'];
                try {
                    $response = $fb->get('/me/taggable_friends?fields=id,name,picture.type(square).width(20).height(20),first_name,last_name,gender&limit=1000&after='.$after);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    return back()->with('error_fb', true);
                }
                $fb_friends = $response->getGraphEdge();
            }else{
                $end = false;
            }
        }

        return $friends;
    }

    public function logout(){
        Auth::logout();
        return back();
    }

    public function newToken(LaravelFacebookSdk $fb){
        if(Auth::check())  $user = Auth::user(); else return redirect('/auth/facebook');
        $token = $user->fb_token;
        if(!$token) return redirect('/auth/facebook');
        $oauth_client = $fb->getOAuth2Client();
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        }catch(Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return redirect('/auth/facebook');
        }

        $user->fb_token = (string) $token;
        $user->save();

        return $token;
    }

    public function checkPermissions($fb){
        $token = $this->newToken($fb);
        $fb->setDefaultAccessToken($token);
        Session::put('fb_user_access_token', (string) $token);
        try {
            $response = $fb->get('/me/permissions');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            Auth::logout();
            return back()->with('error_fb', true);
        }
        $permissions = $response->getGraphEdge();
        $autorization = true;
        foreach($permissions as $permission){
            if($permission['status'] == 'declined') $autorization = false;
        }
        return $autorization;
    }

    function guessGender($name){
        $users = User::where('first_name', $name);
        $male = 0;
        $female = 0;
        foreach($users as $user){
            if($user->gender == 'female'){
                $female++;
            }else{
                $male++;
            }
        }
        if($male >= $female) return 'male'; else return 'female';
    }
}
