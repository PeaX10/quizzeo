<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BackOffice
{

    protected $adminUsers = ['10210583052440872', '10154946865402701'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::check() || !$this->isAdmin(Auth::user()->fb_id)){
            abort(404);
        }

        return $next($request);
    }

    public function isAdmin($user_id){
        return in_array($user_id, $this->adminUsers);
    }
}
