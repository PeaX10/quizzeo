<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# Back
Route::group(['domain' => 'bo.'.env('APP_DOMAIN'), 'middleware' => 'bo'], function () {
    Route::get('/', 'Admin\PageController@index');
    # Tag
    Route::resource('tag', 'Admin\TagController', ['except' => ['show']]);
    # Stats
    Route::get('/stats', 'Admin\PageController@stats');
    Route::get('/stats/{id_quizz}', 'Admin\PageController@statsQuizz');
    Route::group(['prefix' => '/tag/{id}'], function(){
        Route::resource('answer', 'Admin\Tag\AnswerController');
        Route::resource('lang', 'Admin\Tag\LangController', ['except' => ['update', 'index', 'create']]);
        Route::group(['prefix' => '/answer/{anwser}'], function(){
            Route::resource('composant', 'Admin\Tag\ComposantController', ['except' => ['show', 'index']]);
            Route::resource('module', 'Admin\Tag\ModuleController', ['except' => ['show', 'index']]);
        });
        Route::post('/answer/{answer}/pos', 'Admin\Tag\AnswerController@updatePos');
        Route::get('/answer/{answer}/clone', 'Admin\Tag\AnswerController@cloneAnswer');
    });
    Route::resource('quizz', 'Admin\QuizzController', ['except' => ['show']]);
    Route::group(['prefix' => '/quizz/{id}'], function(){
        Route::resource('answer', 'Admin\Quizz\AnswerController');
        Route::resource('lang', 'Admin\Quizz\LangController', ['except' => ['update', 'index', 'create']]);
        Route::group(['prefix' => '/answer/{anwser}'], function(){
            Route::resource('composant', 'Admin\Quizz\ComposantController', ['except' => ['show', 'index']]);
            Route::resource('module', 'Admin\Quizz\ModuleController', ['except' => ['show', 'index']]);
            Route::get('module/{module_id}/setting', 'Admin\Quizz\ModuleController@getSetting');
            Route::post('module/{module_id}/setting', 'Admin\Quizz\ModuleController@postSetting');
            Route::get('module/{module_id}/setting/{data_key}/edit', 'Admin\Quizz\ModuleController@getDataEdit');
            Route::put('module/{module_id}/setting/{data_key}/edit', 'Admin\Quizz\ModuleController@postDataEdit');
            Route::get('module/{module_id}/setting/{data_key}/delete', 'Admin\Quizz\ModuleController@deleteData');
        });
        Route::post('/answer/{answer}/pos', 'Admin\Quizz\AnswerController@updatePos');
        Route::get('/answer/{answer}/clone', 'Admin\Quizz\AnswerController@cloneAnswer');
    });
    Route::get('design', 'Admin\PageController@design');
    Route::get('design/{design_id}/valid', 'Admin\PageController@validDesign');
    Route::get('design/{design_id}/delete', 'Admin\PageController@deleteDesign');
    Route::resource('lang', 'Admin\Quizz\LangController', ['only' => ['edit', 'show', 'update']]);
    Route::resource('translation', 'Admin\TranslationController', ['only' => ['edit', 'index', 'update', 'destroy']]);
});

# Translator
Route::group(['domain' => 'trad.'.env('APP_DOMAIN'), 'middleware' => 'trad'], function () {
    Route::get('/', 'Admin\Trad\PageController@index');
    Route::resource('trans', 'Admin\Trad\TranslationController', ['only' => ['edit', 'index', 'update', 'destroy']]);
});

# Designer
Route::group(['domain' => 'design.'.env('APP_DOMAIN'), 'middleware' => 'design'], function () {
    Route::get('/', 'Admin\Design\DesignerController@index');
    Route::resource('quizzes', 'Admin\Design\QuizzController', ['except' => ['show', 'create']]);
    Route::get('/quizzes/{quizz_id}/design/{answer_id}', 'Admin\Design\DesignerController@design');
    Route::put('/quizzes/{quizz_id}/design/{answer_id}', 'Admin\Design\DesignerController@postDesign');
});

# Front
Route::get('/', 'PageController@index');
Route::post('/', 'PageController@search');
Route::get('/me', 'PageController@me')->middleware('auth');
Route::get('/t/{slug}', 'TagController@index');
Route::get('/q/{slug}', 'QuizzController@index');
Route::get('/t/{slug}/luck', 'TagController@luck');
Route::post('/d/{slug}', 'TagController@doTag');
Route::get('/dq/{slug}', 'QuizzController@doQuizz');
Route::get('/d/{slug}/luck/{friend}', 'TagController@doTagLucky');
Route::get('/r/{slug}', 'TagController@result');
Route::get('/qr/{slug}', 'QuizzController@result');
Route::get('/qrs/{slug}', 'QuizzController@result2');
Route::get('get-more-quizzes', 'PageController@getMoreQuizz');
Route::get('/stats/update/{quizz_id}', 'PageController@updateShareQuizz');
# Pages annexes
Route::get('/terms', 'PageController@terms');
Route::get('/privacy', 'PageController@privacy');
Route::get('/legal', 'PageController@legal');
Route::get('/delete', 'PageController@delete');
Route::get('/delete/confirm', 'PageController@confirmDelete');

# Auth Facebook
Route::get('auth/facebook', 'Auth\FacebookController@login');
Route::get('auth/facebook/callback', 'Auth\FacebookController@callback');
Route::get('sync/friends', 'Auth\FacebookController@syncFriends')->middleware('auth');
Route::get('logout', 'Auth\FacebookController@logout');